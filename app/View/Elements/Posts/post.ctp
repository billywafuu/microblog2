<div id="post-<?=h($post['id'])?>" class="card mb-2 px-0 ">
    <div class="card-body px-0">
    <?= $this->element('Posts/toolbar', array('post' => $post))?>
    <?= $this->element('Users/user_namestrip', array('owner' => $owner)) ?>
            <div class="d-flex">
                <div class="p-3">
                    <small class="text-muted">Posted <?= date_format(new DateTime($post['created']), 'F d, Y') ?> at 
                    <?= date_format(new DateTime($post['created']), 'g:i a') ?>
                    </small>
                    <h5 id="postTitle"><?=h($post['title'])?></h5>
                    <p id="postDescription"> <?=strip_tags($post['body'], '<br></br><br/>')?></p>
                </div>
            </div>
            <?php if($post['image_file_type'] != ""):?>
            <div class="container-fluid text-center">
                <img 
                    src="<?= $this->webroot ?>post/<?= h($post['id'])?>.<?= h($post['image_file_type']) ?>" 
                    style="width: 100%; height:auto"
                    id="postImage"
                >
            </div>
        <?php endif;?>
        <?php if($retweet['deleted'] == 0) :?>
            <?php if($retweet['id'] !== null): ?>
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="p-4">
                                    <h6 id="postTitle"><?=h($retweet['title'])?></h6>
                                    <small id="postDescription"> <?=h($retweet['body'])?></small>
                                </div>
                            </div>
                            <?php if($retweet['image_file_type'] != ""):?>
                            <div class="container-fluid text-center">
                                <img 
                                    src="<?= $this->webroot ?>post/<?= h($retweet['id'])?>.<?=h($retweet['image_file_type'])?>" 
                                    style="width: 100%; height:auto"
                                    id="postImage"
                                >
                            </div>
                        <?php endif;?>
                        <div class="p-4">
                            <a href="<?= $this->base ?>/posts/view/<?=h($retweet['id'])?>" class="badge badge-primary"><i class="fas fa-eye"></i> View original post</a>
                        </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        <?php else :?>
            <div class="p-4">
            <div class="alert alert-warning">
                <h6>Post Unavailable</h6>
                <p>The post that was retweeted was not found or the owner has taken it down.</p>
            </div>
            </div>
        <?php endif ?>
        <?php if(count($likes) > 0):?>
            <div class="container-fluid p-3">
                <small class="text-muted" title="<?= count($likes) ?> users like this post.">
                    <i class="far fa-thumbs-up"></i> <?= count($likes) ?>
                </small>
            </div>
        <?php endif;?>
        <hr>
        <?php
            $hasLiked = false;
            for($i = 0; $i < count($likes); $i++)
            {
                if (isset($likes[$i]['Like']['user_id'])) {
                    if ($likes[$i]['Like']['user_id'] == AuthComponent::user('id')) {
                        $hasLiked = true;
                        break;
                    }
                }
                if (isset($likes[$i]['user_id'])) {
                    if ($likes[$i]['user_id'] == AuthComponent::user('id')) {
                        $hasLiked = true;
                        break;
                    }
                }
            }
        ?>
        <?= $this->element('Posts/action_bar', array(
            'post_id' =>  $post['id'], 
            'hasLiked' => $hasLiked
        ))?>
        <?= $this->element('Posts/comment_box', array('post_id' => $post['id']))?>
        
    </div>
</div>