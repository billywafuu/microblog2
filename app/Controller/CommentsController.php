<?php
/**
 * The controller that manages Comments.
 *
 * @package app.Controller
 */
class CommentsController extends AppController
{
/**
 * Executes beforeFilter()
 */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->loadModel('User');
        $this->loadModel('Post');
    }
 /**
 * This action is responsible for posting new comments to
 * a certain post.
 *
 * This only acknowldeges AJAX post that takes a comment value and
 * and the id of the post. 
 *
 * @return object Returns a JSON object that contains the comment, result and the
 * html.
 */
    public function post()
    {
        if ($this->request->is('ajax')) {
            $this->disableCache();
            $this->autoRender = false;
            $comment = $this->request->data;
            if (empty($comment)) {
                return json_encode(
                    array(
                        "result" => "failed",
                        "description" => "No comment data was sent to the server."
                    )
                );
            }
            if (!$this->Auth->user('id')) {
                return json_encode(
                    array(
                        "result" => "failed",
                        "description" => "A log in is required for this operation."
                    )
                );
            }
            if (
                empty(
                    $this->Post->findById(
                        $comment['post_id']
                    )
                )
            ) {
                return json_encode(
                    array(
                        "result" => "failed",
                        "description" => "The post that you are trying to comment has been removed or does not exist."
                    )
                );
            }
            $comment = array(
                "post_id" => h($comment['post_id']),
                "user_id" => $this->Auth->user('id'),
                "comment" => h(nl2br($comment['comment'])),
                "created" => date("Y-m-d H:i:s")
            );

            $user = $this->User->findById(
                $this->Auth->user('id')
            );
            $this->Comment->save($comment);
            $comment['id'] = $this->Comment->id;
            $view = new View($this, false);
            return json_encode(
                array(
                    "result" => "success",
                    "description" => "The comment was successfully posted.", 
                    "html"  => $view->element(
                        'Comments/user_comment_template',
                        array(
                            'comment' => $comment,
                            'user' => $user['User']
                        )
                    )
                )
            );
        }
        $this->redirect(array('controller' => '/', 'action' => 'index'));
    }
/**
 * Outputs comments as JSON object.
 *
 * @return object Returns a JSON object that contains the comment, result and the
 * html.
 */
    public function load()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            if (!$this->Auth->user('id')) {
                return json_encode(array(
                    "result" => "failed",
                    "description" => "You must be logged in to view comments"
                ));
            }
            $request = $this->request->data;
            $post = $this->Post->findById($request['post_id']);
            if (empty($post)) {
                return json_encode(array(
                    "result" => "failed",
                    "description" => "The post that you requested does not exist."
                ));
            }
            $overallComments = $this->Comment->getComments($request['post_id']);
            if (count($overallComments) == 0) {
                return json_encode(array(
                    "result" => "success",
                    "description" => "There are no comments found on this post.",
                    "html" => ""
                ));
            }
            $Views = new View($this, false);
            $commentPages = ceil(count($overallComments) / MAX_COMMENTS_PER_POST);
            $currentPage = 1 ;
            $currentPage = $request['page'];
            if ($request['page'] < 0) {
                $currentPage = 0;
            }
            $offset = ($currentPage - 1 ) * MAX_COMMENTS_PER_POST;
            $limitComments = $this->Comment->getComments($request['post_id'], MAX_COMMENTS_PER_POST, $offset);
            $html = "";
            for ($i = 0; $i < count($limitComments); $i++) {
                $html = $html . $Views->element('Comments/user_comment_template', array(
                    "user" => $limitComments[$i]['User'],
                    "comment" => $limitComments[$i]['Comment']
                ));
            }
            $loadMoreComments = ($currentPage < $commentPages) ? true : false ;
            return json_encode(array(
                "result" => "success",
                "description" => "Comments retrieved.",
                "count" => count($limitComments),
                "loadMoreComments" => $loadMoreComments,
                "html" => $html,
                "offset" => $offset,
                "page" => $request['page'],
                "maxPage" => $commentPages
            ));
        }
        $this->redirect(array('controller' => '/', 'action' => 'index'));
    }
/**
 * Asynchronous retrieve of comments when viewing individual posts.
 *
 *
 * @return object Returns a JSON object that contains the comment, result and the
 * html.
 */
    public function retrieve()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            if (empty($this->request->data)) {
                return json_encode(array(
                    "result" => "failed",
                    "description" => "No data was sent."
                ));
            }
            if (!$this->Auth->user('id')) {
                return json_encode(array(
                    "result" => "failed",
                    "description" => "You must be logged in to retrieve comments for this post."
                ));
            }
            $this->loadModel('Post');
            $post_id = filter_var($this->request->data['post_id'], FILTER_VALIDATE_INT);
            $timestamp = filter_var($this->request->data['timestamp'], FILTER_VALIDATE_INT);
            if (!$this->Post->find(
                'first',
                array(
                    'conditions' => array(
                        'Post.deleted' => 0,
                        'Post.id'      => $post_id
                    )
                )
             )
            ) {
                return json_encode(array(
                    "result" => "failed",
                    "description" => "The post was not found."
                )); 
            }
            $this->loadModel('Comment');
            $newComments = $this->Comment->getLatest($post_id, $timestamp);
            if (count($newComments) == 0) {
                return json_encode(
                    array(
                    "result" => "success",
                    "update" => "up_to_date",
                    "timestamp" => strtotime(date('Y-m-d H:i:s'))
                    )
                );
            }
            $html = "";
            $Views = new View($this, false);
            for ($i = 0; $i < count($newComments); $i++) {
                $html = $html . $Views->element('Comments/user_comment_template', array(
                    "user" => $newComments[$i]['User'],
                    "comment" => $newComments[$i]['Comment']
                ));
            }
            $latest_timestamp = $newComments[0]['Comment']['timestamp'];
            return json_encode(array(
                "result" => "success",
                "update" => "new_comments",
                "html" => $html,
                "count"  => count($newComments),
                "submittedStamp" => $timestamp,
                "timestamp" => $latest_timestamp,
                "comment" => $newComments
            ));
        }
        $this->autoRender = false;
        return json_encode(array(
            "result" => "invalid_request",
            "description" => "The request was invalid and cannot be processed."
        ));
    }
/**
 * Deletes a comment. Only accepts AJAX request that has a post_id payload.
 *
 * @return object Returns a JSON object that contains the result.
 */
    public function delete(){
        if($this->request->is('ajax')){
            $this->autoRender = false;

            if(empty($this->request->data)){
                return json_encode(array(
                    "result" => "failed",
                    "description" => "No data was sent."
                ));
            }

            if(!$this->Session->check('User.id')){
                return json_encode(array(
                    "result" => "failed",
                    "description" => "You must be logged in to delete comments."
                )); 
            }
            $id = filter_var($this->request->data['id'], FILTER_VALIDATE_INT);
   
            $comment = $this->Comment->findById($id);

            if(!$comment){
                return json_encode(array(
                    "result" => "failed",
                    "description" => "The comment was not found."
                ));
            }

            $post = $this->Post->findById($comment['Comment']['post_id']);

            if(!$post){
                return json_encode(array(
                    "result" => "failed",
                    "description" => "The post was not found."
                ));
            }

            // Check if this comment belongs to the user.
            if($comment['Comment']['user_id'] != $this->Session->read('User.id')){

                // If not, at least check if he/she is the owner of the post. He/she 
                // has the rights to delete the comment. 
                if($post['Post']['user_id'] != $this->Session->read('User.id')){
                    return json_encode(array(
                        "result" => "failed",
                        "description" => "You do not have the rights to delete this comment."
                    ));
                }

            }

            $this->Comment->id = $comment['Comment']['id'];
            $delete = array(
                "deleted" => 1,
                "deleted_at" => date('Y-m-d H:i:s')
            );
            
            $this->Comment->save($delete);

            return json_encode(array(
                "result" => "success",
                "description" => "The comment was successfully deleted.",
                "id" => $comment['Comment']['id']
            ));
            
        }
        $this->autoRender = false;
        return json_encode(array(
            "result" => "invalid_request",
            "description" => "The request was invalid and cannot be processed."
        ));
    }
  
}