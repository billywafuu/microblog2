<div class="container">
<div style="width:450px" class="card mx-auto my-4">
    <div class="card-header">
        <h5> <?= $this->element('back_arrow') ?>Register</h5>
    </div>
    <div class="card-body">
        <form method="POST" id="registrationForm">
            <div class="mb-3">
                <input type="text" name="first_name" class="form-control" placeholder="First Name"
                    value="<?= (isset($form['first_name'])) ? h($form['first_name']) : '' ; ?>"
                >
                <?php if(isset($errors['first_name'])):?>
                    <small class="text-danger"><?=  h($errors['first_name'][0]) ?></small>
                <?php else: ?>
                    <small class="text-muted">Enter your first name</small>
                <?php endif ?>
            </div>
            <div class="mb-3">
                <input type="text" name="middle_name" class="form-control" placeholder="Middle Name"
                    value="<?= (isset($form['middle_name'])) ?  h($form['middle_name']) : '' ; ?>"
                >
                <?php if(isset($errors['middle_name'])):?>
                    <small class="text-danger"><?=  h($errors['middle_name'][0]) ?></small>
                <?php else: ?>
                    <small class="text-muted">Enter your middle name (optional)</small>
                <?php endif ?>
            </div>
            <div class="mb-3">
                <input type="text" name="last_name" class="form-control" placeholder="Last Name" 
                    value="<?= (isset($form['last_name'])) ?  h($form['last_name']) : '' ; ?>"
                >
                <?php if(isset($errors['last_name'])):?>
                    <small class="text-danger"><?=  h($errors['last_name'][0]) ?></small>
                <?php else: ?>
                    <small class="text-muted">Enter your last name</small>
                <?php endif ?>
            </div>
            <div class="mb-3">
                <input type="email" name="email_address" class="form-control" placeholder="Email Address" 
                    value="<?= (isset($form['email_address'])) ?  h($form['email_address']) : '' ; ?>"
                >
                <?php if(isset($errors['email_address'])):?>
                    <small class="text-danger"><?=  h($errors['email_address'][0]) ?></small>
                <?php else: ?>
                    <small class="text-muted">Enter your email address.</small>
                <?php endif ?>
            </div>
            <div class="mb-3">
                <input type="date" name="birthdate" class="form-control"
                    value="<?= (isset($form['birthdate'])) ?  h($form['birthdate']) : '' ; ?>"
                >
                <?php if(isset($errors['birthdate'])):?>
                    <small class="text-danger"><?=  h($errors['birthdate'][0]) ?></small>
                <?php else: ?>
                    <small class="text-muted">Enter your birth date.</small>
                <?php endif ?>
            </div>
            <div class="mb-3">
                <input type="password" minlength="8" id="inputPassword" name="password" class="form-control" placeholder="Password"
                    value="<?= (isset($form['password'])) ?  h($form['password']) : '' ; ?>"
                >
                <?php if(isset($errors['password'])):?>
                    <small class="text-danger"><?=  h($errors['password'][0]) ?></small>
                <?php else: ?>
                    <small class="text-muted">Enter a password with 8 characters or more.</small>
                <?php endif ?>
            </div>
            <div class="mb-3">
                <input type="password" minlength="8" id="inputPassword2" class="form-control" placeholder="Repeat Password">
                <small class="text-muted">Repeat the password that you entered</small>
            </div>
            <div class="alert alert-warning alert-dismissible fade show" id="formAlert" role="alert">
                <strong id="formAlertTitle">Password Mismatch</strong> 
                <p id="formAlertBody"> The password and the repeat password must match. Please try again.</p>
            </div>
            <?php
                $flash = $this->Flash->render();
            ?>
            <?php if($flash != ""):?>
                <div class="alert alert-danger alert-dismissible" id="flashAlert" role="alert">
                    <?= $flash ?>
                </div>
            <?php endif;?>
            <div class="text-right">
                <button type="button" onclick="validateForm()" class="btn btn-primary">Register</button>
            </div>
        </form>
    </div>
</div>
</div>
<script>
    $(document).ready(function()
    {
        $("#formAlert").hide();
    });
    function validateForm()
    { 
        $("#formAlert").hide();
        var password = g('inputPassword').value;
        var repeatPassword = g('inputPassword2').value;
        if (password.length < 8) {
            showFormAlert('Password Too Short',
            'Please choose a password that has 8 characters and above for maximum security.');
            return;
        }    
        if (password != repeatPassword) {
            showFormAlert('Password Mismatch',
            'The password and the repeat password that you entered do not match. Please try again.');
            return;
        }
        g('registrationForm').submit();
    }
    function showFormAlert(title, body)
    {
        $("#formAlertTitle").html(title);
        $("#formAlertBody").html(body);
        $("#formAlert").show();
    }
</script>

