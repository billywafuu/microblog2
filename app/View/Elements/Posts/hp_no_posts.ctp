<div id="hp-no-posts" class="d-flex justify-content-center pa-4">
    <div class="text-center">
        <h5>No posts to show</h5>
        <p>There are no posts to retrieve at this point. Let's try again soon.</p>
        <hr>
    </div>
</div>