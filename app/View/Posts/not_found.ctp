<div class="container-fluid">
        <div class="card microblog-view">
            <div class="card-body">
                <h4>Post Not found</h4>
                <p> The post that you are trying to view was not found. It may have been removed by the owner.</p>
                <small class="text-muted"> If you think this was a mistake, please don't contact us. We don't care.</small>
                <?= $this->element('back_button') ?>
            </div>
        </div>
</div>