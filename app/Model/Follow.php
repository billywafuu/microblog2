<?php 
/**
 * Handles user network (followings and follows) logic.
 *
 * The Follow model contains functions that will help
 * on all followings and follows logics like determining
 * if a user is following a user etc. The model
 * also contains cross model functionalities such as
 * getting all the posts that the user is following.
 *
 * @package       app.Model
 */
class Follow extends AppModel
{
    /**
    *
    *
    * Checks if a user is following another user.
    *
    * @param    int     $user_id This user.
    * @param    int     $following_id the user to check if this user is following .
    * @return   bool    True if following, false if not following.
    *
    */
    public function isUserFollowingUser($user_id, $following_id)
    {
        if (!empty($this->getFollow($user_id, $following_id))) {
            return true;
        }
        return false;
    }
    /**
    *
    * Add a user to follow for this user.
    *
    * @param    int     $user_id This user.
    * @return   array   An array of users.
    *
    */
    public function followUser($user_id, $following_id)
    {
        $newFollow = array(
            "user_id" => $user_id,
            "following_id" => $following_id
        );
        $this->save($newFollow);
    }

    /**
    *
    * Unfollow a user.
    *
    * @param int $user_id The user.
    * @param int $following_id The user to unfollow.
    * @return bool Returns true on operation success.
    *
    */
    public function unfollowUser($user_id, $following_id){
        $follow = $this->getFollow($user_id, $following_id);
        $this->id = $follow['Follow']['id'];
        $unfollow = array(
            "date_unfollowed" => date('Y-m-d H:i:s')
        );
        $this->save($unfollow);
        return true;
    }
    /**
     * Get posts later than the given unix timestamp.
     *
     * Get all the posts of the users that the user_id is
     * following that is later than the given timestamp accompanied
     * with a limit parameter.
     *
     * @param int $user_id The user to retrieve the posts of.
     * @param int $timestamp The timestamp to compare to.
     * @param int $limit The number of posts to return.
     * @return array Returns an array of posts with UNIX_TIMESTAMP(Post.created) > timestamp.
     */
    public function getPostsByTimestamp($user_id, $timestamp, $limit)
    {
        return $this->find('all', array(
            'conditions' => array(
                'Follow.user_id' => $user_id,
                'Follow.date_unfollowed' => null,
                'Post.deleted' => 0,
                'UNIX_TIMESTAMP(Post.created) >' => $timestamp
            ),
            'joins' => array(
                array(
                    'table' => 'posts',
                    'alias' => 'Post',
                    'type'  => 'INNER',
                    'conditions' => array(
                        'Follow.following_id = Post.user_id',
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'Owner',
                    'type'  => 'INNER',
                    'conditions' => array(
                        'Post.user_id = Owner.id'
                    )
                ),
                array(
                    'table' => 'posts',
                    'alias' => 'Retweet',
                    'type'  => 'LEFT',
                    'conditions' => array(
                        'Post.post_id = Retweet.id'
                    )
                )
            ),
            'fields' => array('Follow.following_id',
                'Post.title',
                'Post.body',
                'Post.id',
                'Post.user_id',
                'Post.image_file_type',
                'Post.created',
                'Post.post_id',
                'Owner.first_name',
                'Owner.last_name',
                'Owner.id',
                'Owner.image_file_type',
                'Retweet.title',
                'Retweet.body',
                'Retweet.id',
                'Retweet.user_id',
                'Retweet.image_file_type',
                'Retweet.created',
                'Retweet.deleted'
            ),
            'order' => 'Post.created DESC',
            'limit' => $limit
            ));
    }

    /**
    *
    * Gets the list of users that is being followed by this user.
    *
    * @param int $user_id This user.
    * @return array An array of users.
    *
    */
    public function getUserFollowings($user_id, $limit = null, $offset = null, $search = null)
    {
        if ($limit == null && $offset == null) {
            return $this->find('all', array(
                "conditions" => array(
                    "Follow.user_id" => $user_id,
                    "Follow.date_unfollowed" => null,
                    "User.status" => 1,
                    "User.deleted" => 0,
                    "Follow.following_id !=" => $user_id
                ),
                "joins" => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type'  => 'INNER',
                        'conditions' => array(
                            'Follow.following_id = User.id'
                        )
                    )
                ),
                "fields" => array(
                    "User.first_name",
                    "User.last_name",
                    "User.id",
                    "User.image_file_type",
                    "Follow.date_followed"
                )
            ));
        } else {
            if ($search) {
                return $this->find('all', array(
                    "conditions" => array(
                        "Follow.user_id" => $user_id,
                        "Follow.date_unfollowed" => null,
                        "User.status" => 1,
                        "User.deleted" => 0,
                        "Follow.following_id !=" => $user_id,
                        'OR' => array(
                            array('CONCAT(User.last_name, " ", User.first_name) LIKE' => '%' . $search .'%'),
                            array('CONCAT(User.first_name, " ", User.last_name) LIKE' => '%' . $search .'%'),
                            array('CONCAT(User.first_name, " ", User.middle_name, " ", User.last_name) LIKE' 
                            => '%' . $search . '%'),
                            array('CONCAT(User.last_name, " ", User.first_name, " ", User.middle_name) LIKE' 
                            => '%' . $search . '%'),
                            array('User.email_address LIKE' => '%' . $search . '%')
                        )
                    ),
                    "joins" => array(
                        array(
                            'table' => 'users',
                            'alias' => 'User',
                            'type'  => 'INNER',
                            'conditions' => array(
                                'Follow.following_id = User.id'
                            )
                        )
                    ),
                    "fields" => array(
                        "User.first_name",
                        "User.last_name",
                        "User.id",
                        "User.image_file_type",
                        "Follow.date_followed"
                    ),
                    "limit" => $limit,
                    "offset"=> $offset
                ));
            }else{
                return $this->find('all',
                array(
                    "conditions" => array(
                        "Follow.user_id" => $user_id,
                        "Follow.date_unfollowed" => null,
                        "User.status" => 1,
                        "User.deleted" => 0,
                        "Follow.following_id !=" => $user_id
                    ),
                    "joins" => array(
                        array(
                            'table' => 'users',
                            'alias' => 'User',
                            'type'  => 'INNER',
                            'conditions' => array(
                                'Follow.following_id = User.id'
                            )
                        )
                    ),
                    "fields" => array(
                        "User.first_name",
                        "User.last_name",
                        "User.id",
                        "User.image_file_type",
                        "Follow.date_followed"
                    ),
                    "limit" => $limit, 
                    "offset"=> $offset
                ));
            }
        }
    }
    /**
    *
    * Gets the list of users that is following this user.
    *
    * @param int $user_id This user.
    * @return array An array of users.
    *
    */
    public function getUserFollowers($user_id, $limit = null, $offset = null, $search = null)
    {
        if($limit == null && $offset == null){
            return $this->find('all',array(
                "conditions" => array(
                    "Follow.following_id" => $user_id,
                    "Follow.date_unfollowed" => null,
                    "User.status" => 1,
                    "User.deleted" => 0,
                    "User.id !=" => $user_id 
                ),
                "joins" => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type'  => 'INNER',
                        'conditions' => array(
                            'Follow.user_id = User.id'
                        )
                    )
                ),
                "fields" => array(
                    "User.first_name",
                    "User.last_name",
                    "User.id",
                    "User.image_file_type",
                    "Follow.date_followed"
                )

            ));
        }else{  
            if($search){
                return $this->find('all',array(
                    "conditions" => array(
                        "Follow.following_id" => $user_id,
                        "Follow.date_unfollowed" => null,
                        "User.status" => 1,
                        "User.deleted" => 0,
                        "User.id !=" => $user_id,
                        'OR' => array(
                            array('CONCAT(User.last_name, " ", User.first_name) LIKE' => '%' . $search . '%'),
                            array('CONCAT(User.first_name, " ", User.last_name) LIKE' => '%' . $search . '%'),
                            array('CONCAT(User.first_name, " ", User.middle_name, " ", User.last_name) LIKE' => '%' . $search . '%'),
                            array('CONCAT(User.last_name, " ", User.first_name, " ", User.middle_name) LIKE' => '%' . $search . '%'),
                            array('User.email_address LIKE' => '%' . $search . '%')
                        )
                    ),
                    "joins" => array(
                        array(
                            'table' => 'users',
                            'alias' => 'User',
                            'type'  => 'INNER',
                            'conditions' => array(
                                'Follow.user_id = User.id'
                            )
                        )
                    ),
                    "limit" => $limit, 
                    "offset" => $offset,
                    "fields" => array(
                        "User.first_name",
                        "User.last_name",
                        "User.id",
                        "User.image_file_type",
                        "Follow.date_followed"
                    )
                ));
            }else{
                return $this->find('all',array(
                    "conditions" => array(
                        "Follow.following_id" => $user_id,
                        "Follow.date_unfollowed" => null,
                        "User.status" => 1,
                        "User.deleted" => 0,
                        "User.id !=" => $user_id 
                    ),
                    "joins" => array(
                        array(
                            'table' => 'users',
                            'alias' => 'User',
                            'type'  => 'INNER',
                            'conditions' => array(
                                'Follow.user_id = User.id'
                            )
                        )
                    ),
                    "limit" => $limit, 
                    "offset" => $offset,
                    "fields" => array(
                        "User.first_name",
                        "User.last_name",
                        "User.id",
                        "User.image_file_type",
                        "Follow.date_followed"
                    )
                ));
            }
        }
    }
    /**
    *
    * Gets the record of the follow data between users.
    *
    * @param int $user_id This user.
    * @param int $following_id The other user.
    * @return row The record.
    *
    */
    public function getFollow($user_id, $following_id)
    {
        return $this->find(
            'first',
            array(
            'conditions' => array(
                'Follow.user_id' => $user_id,
                'Follow.following_id' => $following_id,
                'Follow.date_unfollowed' => null
                )
            )
        );
    }
    /**
    *
    * A function that returns all the posts from the users that this user is following.
    *
    * @param int $user_id The user id of the current user.
    * @param int $limit The number of records that will be returned.
    * @param int $offset The index where the cursor will start retrieving data.
    * @return array Returns an array of posts.
    */
    public function getFollowingPost($user_id, $limit = null, $offset = null)
    {
        if ($limit == null && $offset == null) {
            return $this->find('all', array(
                'conditions' => array(
                    'Follow.user_id' => $user_id,
                    'Follow.date_unfollowed' => null,
                    'Post.deleted' => 0,
                ),
                'joins' => array(
                    array(
                        'table' => 'posts',
                        'alias' => 'Post',
                        'type'  => 'INNER',
                        'conditions' => array(
                            'Follow.following_id = Post.user_id',
                        )
                    ),
                    array(
                        'table' => 'users',
                        'alias' => 'Owner',
                        'type'  => 'INNER',
                        'conditions' => array(
                            'Post.user_id = Owner.id'
                        )
                    ),
                    array(
                        'table' => 'posts',
                        'alias' => 'Retweet',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'Post.post_id = Retweet.id'
                        )
                    )
                ),
                'fields' => array('Follow.following_id',
                    'Post.title',
                    'Post.body',
                    'Post.id',
                    'Post.user_id',
                    'Post.image_file_type',
                    'Post.created',
                    'Post.post_id',
                    'Owner.first_name',
                    'Owner.last_name',
                    'Owner.id',
                    'Owner.image_file_type',
                    'Retweet.title',
                    'Retweet.body',
                    'Retweet.id',
                    'Retweet.user_id',
                    'Retweet.image_file_type',
                    'Retweet.created',
                    'Retweet.deleted'
                ),
                'order' => 'Post.created DESC'
                ));
        }

        return $this->find('all', array(
            'conditions' => array(
                'Follow.user_id' => $user_id,
                'Follow.date_unfollowed' => null,
                'Post.deleted' => 0,
            ),
            'joins' => array(
                array(
                    'table' => 'posts',
                    'alias' => 'Post',
                    'type'  => 'INNER',
                    'conditions' => array(
                        'Follow.following_id = Post.user_id',
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'Owner',
                    'type'  => 'INNER',
                    'conditions' => array(
                        'Post.user_id = Owner.id'
                    )
                ),
                array(
                    'table' => 'posts',
                    'alias' => 'Retweet',
                    'type'  => 'LEFT',
                    'conditions' => array(
                        'Post.post_id = Retweet.id'
                    )
                )
            ),
            'fields' => array('Follow.following_id',
                'Post.title',
                'Post.body',
                'Post.id',
                'Post.user_id',
                'Post.image_file_type',
                'Post.created',
                'Post.post_id',
                'Owner.first_name',
                'Owner.last_name',
                'Owner.id',
                'Owner.image_file_type',
                'Retweet.title',
                'Retweet.body',
                'Retweet.id',
                'Retweet.user_id',
                'Retweet.image_file_type',
                'Retweet.created',
                'Retweet.deleted'
        ),
            'order' => 'Post.created DESC',
            'limit' => $limit,
            'offset' => $offset
            ));
    }
}