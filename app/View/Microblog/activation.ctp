<?php
    if($this->Flash->render()!=""){
        return;
    }
?>
<div class="container">
    <div class="card w-75 mt-4 mx-auto">
        <div class="card-body">
            <h5 class="card-title"><i class="fas fa-envelope"></i> We sent you an email</h5>
            <p class="card-text">An email has been sent to <?= h($this->Session->consume('User.activation')) ?> for activation. Check your inbox and click the provided link to activation your account for use.</p>
        </div>
    </div>
</div>
