<?php
/**
 * The controller that allows unauthenticated transactions.
 *
 * The controllers that allows unauthenticated or externals
 * transactions such as activation, home page, registering for new account, 
 * forgot passwords and login.
 * 
 * @package app.Controller
 */
class MicroblogController extends AppController
{
    public function beforeFilter()
    {
        $this->Auth->allow(
            'index',
            'register',
            'login',
            'logout',
            'forgotpassword',
            'activation',
            'resetpassword'
        );
        parent::beforeFilter();
    }
    /**
    * The default home controller action.
    *
    */
    public function index()
    {
    }
    /**
    * Activates a user account with matching id and activation token.
    * When the action receives a matched id and activation token, it
    * will retrieve the corresponding user_id on its record and activate
    * it.
    *
    * @param string $id The id of the activation record in the database.
    * @param string $activation_token The matching token of the id. 
    */
    public function activation($id = null, $activation_token = null)
    {
        if ($id !== null && $activation_token !== null) {
            $this->loadModel('Activation');
            $this->loadModel('Follow');
            if (!$this->Activation->activate($id, $activation_token)) {
                $this->Flash->error(
                    'The system could not find the account that you wish to activate.',
                    array(
                        "key" => "error"
                    )
                );
                $this->redirect(array('controller' => '/','action' => 'login'));
                return;
            } else {
                $this->Flash->success(
                    'Your account has been successfully activated. You may now login your account.',
                    array(
                        "key" => "success"
                    )
                );
                $this->redirect(array('controller' => '/','action' => 'login'));
                return;
            }
        }
        if (!$this->Session->check('User.activation')) {
            $this->redirect(
                array('controller' => '/','action' => 'index')
            );
        }
    }
    /**
    * The login action that receives email_address and password
    * as payload. The hash checking of this action uses BCRYPT.
    */
    public function login()
    {
        if ($this->Auth->user('id')) {
            $this->redirect(
                array(
                    "controller" => "/",
                    "action"     => "index"
                )
            );
        }
        if ($this->request->is('post')) {
            $this->loadModel('User');
            $email_address = $this->request->data('email_address');
            $password      = $this->request->data('password');
            if ($email_address === null || $password === null) {
                $this->Flash->error(
                    'Please enter your email address and password.',
                    array("key" => "error")
                );
                return;
            }
            $user = $this->User->find('first', array(
                "conditions" => array(
                    "User.email_address" => $email_address
                )
            ));
            if (empty($user)) {
                $this->Flash->error(
                    LOGIN_ERROR_MESSAGE,
                    array("key" => "error")
                );
                return;
            }
            if (!password_verify($password, $user['User']['password'])) {
                $this->Flash->error(
                    LOGIN_ERROR_MESSAGE,
                    array("key" => "error")
                );
                return;
            }
            if ($user['User']['status'] == 0) {
                $this->Session->write('User.activation', $email_address);
                $this->redirect(array('controller' => '/','action' => 'activation'));
                return;
            }
            $this->Session->write('Search.target', 'User');
            $this->Auth->login(
                array(
                    'id' => $user['User']['id'],
                    'email_address' => $user['User']['email_address'],
                    'first_name' => $user['User']['first_name'],
                    'middle_name' => $user['User']['middle_name'],
                    'last_name' => $user['User']['last_name'],
                    'birthdate' => $user['User']['birthdate'],
                    'image_file_type' => $user['User']['image_file_type']
                )
            );
            return $this->redirect($this->Auth->redirectUrl());
        }
    }
    /**
    * The logout action that logs out the user.
    */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    /**
    * The register action that registers the user.
    */
    public function register()
    {
        if ($this->Auth->user('id')) {
            $this->Flash->warning(
                MUST_BE_LOGGED_OUT,
                array("key" => "warning")
            );
            $this->redirect(
                array(
                    "controller" => "/",
                    "action"     => "index"
                )
            );
        }
        if ($this->request->is('post')) {
            $this->loadModel('User');
            $this->User->set(
                array("User" => $this->request->data)
            );
            if (!$this->User->validates()) {
                $this->Flash->error(
                    FORM_ERRORS,
                    array(
                        "key" => "error"
                    )
                );
                $this->set(
                    'errors',
                    $this->User->validationErrors
                );
                $this->set(
                    'form',
                    $this->request->data
                );
                return;
            }
            $this->loadModel('Activation');
            $this->loadModel('Utility');
            $email_address = $this->request->data['email_address'];
            if (!$this->User->isEmailAddressAvailable($email_address)) {
                $this->Flash->error(
                    'The email address is not available 
                    for use as someone is already using it. 
                    If you think that this is a mistake, 
                    please do not contact us.',
                    array("key" => "error")
                );
                $this->set('form', $this->request->data);
                return;
            }
            $user = array(
                "first_name"    => $this->request->data['first_name'],
                "middle_name"   => $this->request->data['middle_name'],
                "last_name"     => $this->request->data['last_name'],
                "email_address" => $this->request->data['email_address'],
                "password"      => password_hash(
                    $this->request->data['password'], 
                    PASSWORD_DEFAULT
                ),
                "birthdate"    => $this->request->data['birthdate']
            );
            if (!$this->User->save($user)) {
                $this->Flash->error(
                    CANNOT_SAVE_ACCOUNT,
                    array(
                        "key" => "error"
                    )
                );
                $this->set('form', $this->request->data);
                return;
            }

            if (
                !$this->Activation->send(
                    $this->User->id,
                    $this->Utility->generateVarchar(),
                    $this->Utility->generateVarchar()
                )
            ) {
                $this->Flash->error(
                    CANNOT_SEND_EMAIL,
                    array(
                        "key" => "error"
                    )
                );
            }

            $this->Session->write('User.activation', $email_address);
            $this->redirect(
                array(
                    'controller' => 'microblog','action' => 'activation'
                )
            );
            return;
        }
    }
    /**
    * The forgot password action that processess the forgot
    * password operations.
    */
    public function forgotPassword()
    {
        if ($this->Auth->user('id')) {
            $this->Flash->warning(
                MUST_BE_LOGGED_OUT,
                array("key" => "warning")
            );
        }
        if ($this->request->is('post')) {
            $email_address = $this->request->data('inputEmailAddress');
            if ($email_address === null) {
                $this->Flash->error(
                    INVALID_EMAIL_ADDRESS,
                    array(
                        "key" => "warning"
                    )
                );
                return;
            } 
            if (!filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
                $this->Flash->error(
                    INVALID_EMAIL_ADDRESS,
                    array(
                        "key" => "warning"
                    )
                );
                return;
            }
            $this->loadModel('ForgotPassword');
            $this->loadModel('User');
            $user = $this->User->getWithEmailAddress($email_address);
            if (empty($user)) {
                $this->Flash->warning(
                    NO_ACCOUNT_FOUND,
                    array("key" => "warning")
                );
                return;
            }
            $lastRequest = $this->ForgotPassword->getCurrentRequest($email_address);
            if (!empty($lastRequest)) {
                $dateNow     = strtotime(date('Y-m-d H:i:s'));
                $dateRequest = strtotime($lastRequest['ForgotPassword']['created']);
                $minutes = round(($dateNow - $dateRequest) / 60, 2);
                if ($minutes < 5) {
                    $this->Flash->error(
                        'Please wait for ' . (5 - $minutes) .
                        ' minutes more before requesting for another one. 
                        Please wait for the email as it sometimes take time to arrive.',
                        array(
                            "key" => "warning"
                        )
                    );
                    return;
                }
                $this->ForgotPassword->delete($lastRequest['ForgotPassword']['id']);
            }
            if ($this->ForgotPassword->send($email_address)) {
                $this->Flash->success(
                    'We have sent an email on your email address. 
                    Kindly check your inbox and click the reset password link to reset your password.',
                    array(
                        "key" => "success"
                    )
                );
                return;
            } else {
                $this->Flash->success(
                    'There was an error while trying to send an email. Please try again later.',
                    array(
                        "key" => "error"
                    )
                );
                return;
            }
        }
    }
    /**
    * Resets the password of an account when the id and reset token
    * matched.
    *
    * @param string $id The id of the reset password record in the database.
    * @param string $reset_token The matching token of the id. 
    */
    public function resetPassword($id = null, $reset_token = null)
    {
        $this->loadModel('ForgotPassword');
        if ($this->request->is('post')) {
            $this->loadModel('User');
            $newPassword = $this->request->data['inputPassword'];
            $user = $this->User->getWithId($this->Session->read('ForgotPassword.user_id'));
            if (empty($user)) {
                $this->Flash->error(
                    'The account that you are trying to reset the password does not exist. Please try again.',
                    array(
                        "key" => "success"
                    )
                );
                return;
            }
            $this->User->id = $user['User']['id'];
            $updatedPassword = array(
                "password" => password_hash($newPassword, PASSWORD_DEFAULT)
            );
            $this->User->save($updatedPassword); 
            $this->Flash->success(
                'You have successfully changed your password. You may now log in your account',
                array(
                    "key" => "success"
                )
            );
            $this->ForgotPassword->delete($id);
            $this->Session->consume('ForgotPassword.user_id');
            $this->redirect(array('controller' => '/','action' => 'login'));
            return;
        }
        if ($id == null || $reset_token == null) {
            $this->Flash->error(
                'The credentials that you passed is invalid. Please try again',
                array(
                    "key" => "error"
                )
            );
            $this->redirect(array('controller' => '/','action' => 'forgotpassword'));
            return;
        }
        $forgotPassword = $this->ForgotPassword->findById($id);
        if (empty($forgotPassword)) {
            $this->Flash->error(
                'An invalid request has been received. If you clicked a link from your email to reset your password, 
                kindly request for another by entering your email address below.',
                array(
                    "key" => "error"
                )
            );
            $this->redirect(array('controller' => '/','action' => 'forgotpassword'));
            return;
        }
        if ($forgotPassword['ForgotPassword']['reset_token'] != $reset_token) {
            $this->Flash->error(
            'An invalid request has been received. If you clicked a link from your email to reset your password, 
            kindly request for another by entering your email address below.',
                array(
                    "key" => "error"
                )
            );
            $this->redirect(array('controller' => '/','action' => 'forgotpassword'));
            return;
        }
        $requestDate = strtotime($forgotPassword['ForgotPassword']['created']);
        $dateNow     = strtotime(date('Y-m-d H:i:s'));
        $minutes = round(($dateNow - $requestDate) / 60, 2);
        if ($minutes > 60) {
            $this->Flash->error('This password reset request has expired. 
            Kindly request for another by entering your email address below.',
            array("key" => "error"));
            $this->ForgotPassword->delete($id);
            $this->redirect(array('controller' => '/','action' => 'forgotpassword'));
            return;
        }
        $this->Session->write('ForgotPassword.user_id', $forgotPassword['ForgotPassword']['user_id']);
    }
}
