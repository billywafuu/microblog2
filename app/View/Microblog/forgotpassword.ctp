<?= $this->Flash->render('fp_success') ?>
<?= $this->Flash->render('fp_error') ?>
<div class="container-fluid">
    <div class="card microblog-view">
        <div class="card-body">
            <h4>Forgot Password</h4>
            <p>Enter the email address of your account that you forgot the password. We will send an email containing the password reset link for the account.</p>
            <form method="POST">
                <div class="mb-2">
                    <input class="form-control" type="email" name="inputEmailAddress" placeholder="Your email address" required>
                    <small class="text-muted">Enter your email address here. If you do not receive any email within 5 minutes, you may request another one from here.</small>                    
                </div>
                <div class="text-right">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>        
        </div>
    </div>
</div>