<div class="container-fluid">
        <div class="card microblog-view">
            <div class="card-body">
                <h4>User not found</h4>
                <p> The user that you are trying to access the network of is not found. </p>
                <small class="text-muted"> The user might have made his/her network unsearchable by other users.</small>
                <?= $this->element('back_button') ?>
            </div>
        </div>
</div>