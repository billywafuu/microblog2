<div class="jumbotron">
  <h1 class="display-4">Microblog 2</h1>
  <p class="lead">A simple social media like platform that could destroy Twitter and Facebook. Single handed.</p>
  <p class="lead text-right">
    <a class="btn btn-primary btn-lg" href="<?php Router::url('/', true); ?>register" role="button">Sign Up Now!</a>
  </p>
  
</div>