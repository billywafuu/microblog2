<?= $this->element('Posts/retweeter') ?>
<div class="container-fluid mt-2">
    <div id="post-<?=h($post['Post']['id'])?>" class="card mb-2 px-0 microblog-view">
        <div class="card-body px-0">
        <?= $this->element('Posts/toolbar', array('post' => $post['Post']))?>
        <?= $this->element('Users/user_namestrip', array('owner' => $post['Owner'])) ?>
            <div class="d-flex">
                <div class="p-4">
                    <h5 id="postTitle"><?=h($post['Post']['title'])?></h5>
                    <p id="postDescription"> <?=strip_tags($post['Post']['body'], '<br></br><br/>')?></p>
                </div>
            </div>
            <?php if($post['Post']['image_file_type'] != ""):?>
            <div class="container-fluid text-center">
                <img 
                    src="<?= $this->webroot ?>post/<?= h($post['Post']['id'])?>.<?= h($post['Post']['image_file_type'])?>" 
                    style="width: 100%; height:auto" id="postImage"
                >
            </div>
            <?php endif;?>
            <?php if($post['Retweet']['deleted'] == 0) :?>
                <?php if($post['Retweet']['id'] !== null): ?>
                <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="p-4">
                                <h6><?=h($post['Retweet']['title'])?></h6>
                                <small> <?=h($post['Retweet']['body'])?></small>
                            </div>
                        </div>
                        <?php if($post['Retweet']['image_file_type'] != ""):?>
                            <div class="container-fluid text-center">
                                <img 
                                    src="<?= $this->webroot ?>post/<?=h($post['Retweet']['id'])?>.<?=h($post['Retweet']['image_file_type'])?>" 
                                    style="width: 100%; height:auto"
                                >
                            </div>
                        <?php endif;?>
                        <div class="p-4">
                            <a href="<?= $this->base ?>/posts/view/<?=h($post['Retweet']['id'])?>" class="badge badge-primary"><i class="fas fa-eye"></i> View original post</a>
                        </div>
                        <?php if(count($post['Likes']) > 0):?>
                            <div class="container-fluid p-3">
                                <small class="text-muted" title="<?= h(count($post['Likes']))?> users like this post.">
                                    <i class="far fa-thumbs-up"></i> <?= h(count($post['Likes']))?>
                                </small>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <?php endif ?>
            <?php else :?>
                <div class="p-4">
                    <div class="alert alert-warning">
                        <h6>Post Unavailable</h6>
                        <p>The post that was retweeted was not found or the owner has taken it down.</p>
                    </div>
                </div>
            <?php endif ?>
            <hr>
            <?php
                $hasLiked = false;
                for($i = 0; $i < count($post['Likes']); $i++)
                {
                    if ($post['Likes'][$i]['user_id'] == AuthComponent::user('id')) {
                        $hasLiked = true;
                        break;
                    }
                }
            ?>
            <?= $this->element(
                'Posts/action_bar', array(
                    "hasLiked" => $hasLiked, 
                    "post_id" => h($post['Post']['id']
                    )
                )
            )?>  
            <div class="container p-4">
                <div class="alert alert-light" role="alert">
                    <p><strong><i class="fas fa-comments"></i> Start a conversation on this post </strong> <br>New comments are automatically retrieved.</p>
                </div>         
            </div>
            <?= $this->element('Posts/ajax_comment_box', array("post_id" => h($post['Post']['id'])))?>
        </div>
    </div>
</div>

<script>
var lastUpdatedTimestamp = 0;
var commentRetriever;
var commentBox;
$(document).ready(() => {
    commentBox = $("#post-<?= $post['Post']['id'] ?>-comment-box");
    $("#a-load-more-comments-<?= $post['Post']['id'] ?>").hide();
    createSpinner('<?=$post['Post']['id'] ?>');
    commentRetriever = setInterval(()=>{ 
        $.ajax({
            url: bu() + 'comments/retrieve',
            data: { 
                post_id: '<?= $post['Post']['id'] ?>',
                timestamp: lastUpdatedTimestamp
            }, 
            datatype: 'json',
            type: 'post',
            success: (response) => {
                response = JSON.parse(response);
                destroySpinner('<?=$post['Post']['id'] ?>');
                $("#a-load-more-comments-<?= $post['Post']['id'] ?>").show();
                console.log(response);
                if(response.result == "success"){
                    if(response.update == "new_comments"){
                        commentBox.prepend(response.html);
                        lastUpdatedTimestamp = response.timestamp;
                    }
                }
            }, 
            error: ()=> {
                $.dialog("Unfortunately, it seems that we cannot pull new comments for this thread.", "Dang!");
                clearInterval(commentRetriever);
            }
        })
    }, 2000);
})
</script>