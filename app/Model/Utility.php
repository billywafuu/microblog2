<?php 
App::uses('AppModel', 'Model');
/**
 * A helper model that provides useful functions.
 *
 * @package       app.Model
 */
class Utility extends AppModel
{
    /**
     *
     * Generates a random 16 length varchar by default.
     * The parameter can be overriden to any length.
     *
     * @param int $length The length of varchar to return.
     * @return string Returns a random varchar with specified length.
     */
    public function generateVarchar($length = 16)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyz12345678901234567890';
        $id = array();
        $numlen = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $id[$i] = $chars[rand(0, $numlen - 1)];
        }
        return strtoupper(implode($id));
    }
}
