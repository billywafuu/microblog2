<div class="container-fluid">
    <div class="d-flex flex-row">
        <div style="height:120px; width:auto; overflow: hidden">
            <img src="<?= $this->webroot ?>post/<?= h($post['id']) ?>.<?= h($post['image_file_type']) ?>"
            class="img-fluid">
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-2">
            <h5><?= h($post['title']) ?></h5>
            <small class="text-muted">Posted on <?= h($post['created']) ?></small>
            <p class="text-muted"><?= h($post['body']) ?></p>
            <a href="<?= $this->base ?>/posts/view/<?= h($post['id']) ?>" class="badge badge-primary"><i class="fas fa-eye"></i> View Post</a>
        </div>
    </div>
    <hr>
</div>