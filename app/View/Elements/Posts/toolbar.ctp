<?php if(AuthComponent::user('id') == $post['user_id']):?>
    <button type="button" onclick="deletePost('<?=$post['id']?>')" class="close p-2" aria-label="Close">
            <span aria-hidden="true">&times;</span>
    </button>
    <button type="button" onclick="editPost('<?=$post['id']?>')" class="close p-2" aria-label="Close">
        <span aria-hidden="true"><i class="fas fa-pen"></i></span>
    </button>
<?php endif;?>
<a type="button" href="<?= $this->base ?>/posts/view/<?= $post['id']?>" class="close p-2" aria-label="Close">
    <span aria-hidden="true"><i class="fas fa-eye"></i></span>
</a>