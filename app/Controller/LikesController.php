<?php
/**
 * The controller that manages Likes.
 * 
 * @package app.Controller
 */
class LikesController extends AppController
{
    public function beforeFilter()
    {
        parent::beforeFilter();
    }
/**
 * Like/unlike a certain post. This action receives a post_id as
 * the payload.
 *
 * @return object Returns a JSON object that contains the result.
 */
    public function like()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('Post');
            $this->disableCache();
            $this->autoRender = false;
            if (empty($this->request->data)) {
                return json_encode(
                    array(
                        "result" => "failed",
                        "description" => "No data was sent to the server."
                    )
                );
            }

            if (!$this->Auth->user('id')) {
                return json_encode(
                    array(
                        "result" => "failed",
                        "description" => "A log in is required for this operation."
                    )
                );
            }

            if (empty($this->Post->findById($this->request->data['post_id']))) {
                return json_encode(
                    array(
                        "result" => "failed",
                        "description" => "The post that you are trying to 
                        like/unlike has been removed or does not exist."
                    )
                );
            }

            $like = $this->Like->getLike(
                $this->Auth->user('id'),
                $this->request->data['post_id']
            );
            
            if (empty($like)) {

                $like = array(
                    "user_id" => $this->Auth->user('id'),
                    "post_id" => $this->request->data['post_id'],
                    "created" => date('Y-m-d H:i:s')
                );

                $this->Like->save($like);

                return json_encode(
                    array(
                        "result" => "success",
                        "state"  => "liked",
                        "description" => "Successfully liked a post."
                    )
                );

            } else {
                $this->Like->id = $like['Like']['id'];
                $like = array(
                    "deleted"    => 1,
                    "deleted_at" => date('Y-m-d H:i:s')
                );
                $this->Like->save($like);
                return json_encode(
                    array(
                        "result" => "success",
                        "state"  => "unliked",
                        "description" => "Successfully unliked a post."
                    )
                );
            }
        }
        $this->redirect(array('controller' => '/', 'action' => 'index'));
    }
}