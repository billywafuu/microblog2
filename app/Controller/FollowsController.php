<?php
/**
 * The controller that manages flows regarding Follows.
 * 
 * @package app.Controller
 */
class FollowsController extends AppController
{
    public function beforeFilter()
    {
        parent::beforeFilter();
    }
/**
 * Follows a certain user. This action receives the user_id of the user that will
 * be followed.
 *
 * @return object Returns a JSON object that containing the result.
 */
    public function follow()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->loadModel('User');
            $user_id = $this->request->data['user_id'];
            $user = $this->User->getWithId($user_id);
            if (empty($user)) {
                return json_encode(
                    array(
                        "result" => "failed",
                        "description" => "No user found with that id."
                    )
                );
            }
            if ($user['User']['status'] == 0) {
                return json_encode(
                    array(
                        "result" => "failed",
                        "description" => "The user needs activation first."
                    )
                );
            }
            if ($this->Follow->isUserFollowingUser($this->Auth->user('id'), $user_id)) {
                $this->Follow->unfollowUser($this->Auth->user('id'), $user_id);
                return json_encode(
                    array(
                        "result" => "unfollow_success",
                        "description" => "The unfollow was successful.",
                        "userName" => $user['User']['first_name'] . ' ' . $user['User']['last_name']
                    )
                );
            } else {
                $this->Follow->followUser($this->Auth->user('id'), $user_id);
                return json_encode(
                    array(
                        "result" => "follow_success",
                        "description" => "The follow was successful.",
                        "userName" => $user['User']['first_name'] . ' ' . $user['User']['last_name']
                    )
                );
            }
        }
    }
}
