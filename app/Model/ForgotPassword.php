<?php
App::uses('CakeEmail', 'Network/Email');
/**
 * Handles forgot password logics.
 * @package       app.Model
 */
class ForgotPassword extends AppModel
{
    /**
    *
    * Sends a reset password email with a reset link to a certain user's email.
    * @param string $email_address The email address that the system will send the email to.
    * @return array True when sent, false when failed.
    */
    public function send($email_address)
    {
        $userModel    = ClassRegistry::init('User');
        $utilityModel = ClassRegistry::init('Utility');
        $user = $userModel->getWithEmailAddress($email_address);
        if (empty($user)) {
            return false;
        }
        if (empty($user['User']['email_address'])) {
            return false;
        }
        generateId:
        $id = $utilityModel->generateVarchar();
        if ($this->findById($id)) {
            goto generateId;
        }
        $reset_token = $utilityModel->generateVarchar(); 
        $Email = new CakeEmail();
        $Email->config('smtp');
        $forgotPassword = array(
            "user_id" => $user['User']['id'],
            "id"      => $id, 
            "reset_token" => $reset_token
        );
        $this->save($forgotPassword);
        $Email->template('forgotpassword')
            ->viewVars(
                array(
                    'id' => $id,
                    'reset_token' => $reset_token,
                    'first_name' => $user['User']['first_name'],
                    'user_id' => $user['User']['id']
                )
            )
            ->emailFormat('html')
            ->to($user['User']['email_address'])
            ->subject('Microblog Forgot Password')
            ->from('support@localhost', 'Microblog')
            ->send();

        return true;
    }
    /**
    *
    * Gets the current request for reset password.
    * @param  string $email_address The email address that the system will send the email to.
    * @return row The forgot password row data. Null if no record found.
    */
    public function getCurrentRequest($email_address)
    {
        $userModel = ClassRegistry::init('User');
        $user      = $userModel->getWithEmailAddress($email_address);
        if (empty($user)) {
            return null;
        }
        return $this->find('first', array(
            "conditions" => array(
                "ForgotPassword.user_id" => $user['User']['id']
            )
        ));
    }
}