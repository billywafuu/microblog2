<?php
/**
 * The controller that manages Users.
 *
 * @package app.Controller
 */
class UsersController extends AppController
{
    public function beforeFilter()
    {
        parent::beforeFilter();
    }
    /**
    * The main index of the User controller.
    *
    */
    public function index()
    {
        $this->autoRender = false;
        echo "Index";
    }
    /**
    * The settings action that renders the settings
    * view.
    *
    */
    public function settings()
    {
        if ($this->request->is('post')) {
            $user = $this->User->findById(
                $this->Auth->user('id')
            );
            $this->User->set(
                array("User" => $this->request->data)
            );
            if (!$this->User->validates()) {
                $this->Flash->error(
                    FORM_ERRORS,
                    array(
                    "key" => "error"
                    )
                );
                $this->set('errors', $this->User->validationErrors);
                return;
            }
            $this->loadModel('Utility');
            $this->loadModel('Activation');
            if (!(bool)strtotime($this->request->data['birthdate'])) {
                $this->Flash->error(
                    DATE_ERROR,
                    array(
                        "key" => "error"
                    )
                );
                return;
            }
            $this->User->id = $this->Auth->user('id');
            if ($this->User->save($this->request->data)) {
                $this->Session->write('Auth.User.first_name', $this->request->data['first_name']);
                $this->Session->write('Auth.User.middle_name', $this->request->data['middle_name']);
                $this->Session->write('Auth.User.last_name', $this->request->data['last_name']);
                $this->Session->write('Auth.User.birthdate', $this->request->data['birthdate']);
            }
            if ($user['User']['email_address'] != $this->request->data['email_address']) {
                $lock = array(
                    "status" => 0,
                );
                $this->User->save($lock);
                if (
                    $this->Activation->send(
                        $this->User->id,
                        $this->Utility->generateVarchar(),
                        $this->Utility->generateVarchar()
                    )
                ) {
                    $this->Session->write(
                        'User.activation',
                        $this->request->data['email_address']
                    );
                    $this->Flash->success(
                        EMAIL_CHANGE,
                        array(
                            'key' => 'success'
                        )
                    );
                    $this->loadModel('Follow');
                    $this->Follow->deleteAll(
                        array(
                            'Follow.user_id' => $this->Auth->user('id')
                        ),
                        false
                    );
                    $this->redirect(
                        array(
                        'controller' => '/',
                        'action' => 'logout'
                        )
                    );
                    return;
                }
            }
            $this->Flash->success(
                SAVE_SUCCESSFUL,
                array(
                    'key' => 'success'
                )
            );
            $this->redirect(array('controller' => 'users', 'action' => 'settings'));
        }
    }
    /**
    * The change password action that renders the change password
    * view.
    *
    */
    public function changePassword()
    {
        if ($this->request->is('post')) {
            $currentPassword = $this->request->data['inputCurrentPassword'];
            $newPassword     = $this->request->data['inputPassword'];
            if (empty($currentPassword)) {
                $this->Flash->error(
                    'The current password that you entered is incorrect. 
                Please enter your current password to change your password', 
                    array(
                        "key" => "error"
                    )
                );
                return;
            }
            $user = $this->User->getWithId($this->Auth->user('id'));
            if (empty($user)) {
                $this->Flash->error(
                    'The current password that you entered is incorrect. 
                    Please enter your current password to change your password', 
                    array(
                        "key" => "cp_error"
                    )
                );
                return;
            }
            if (!password_verify($currentPassword, $user['User']['password'])) {
                $this->Flash->error(
                    'The current password that you entered is incorrect. 
                    Please enter your current password to change your password', 
                    array(
                        "key" => "cp_error"
                    )
                );
                return;
            }
            if ($currentPassword == $newPassword) {
                $this->Flash->error(
                    'You cannot use your current password as your new password. 
                    Please create a new and unique password.',
                    array("key" => "error")
                );
                return;
            }
            $this->User->id = $this->Auth->user('id');
            $updatedPassword = array(
                "password" => password_hash($newPassword, PASSWORD_DEFAULT)
            );
            $this->User->save($updatedPassword);
            $this->Flash->success(
                'You have successfully changed your password.',
                array("key" => "cp_success")
            );
            return;
        }
    }
    /**
    * The network action that renders the network
    * view.
    *
    */
    public function network()
    {
        $this->loadModel('Follow');
        $this->set(
            'viewData',
            array(
                'followingsCount' => count($this->Follow->getUserFollowings(
                    $this->Auth->user('id')
                )),
                'followersCount'  => count($this->Follow->getUserFollowers(
                    $this->Auth->user('id')
                ))
            )
        );
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $request = $this->request->data;
            if (empty($request)) {
                return json_encode(array(
                    "result" => "failed",
                    "description" => "Empty request data.",
                    "html" => "",
                    "pagination" => ""
                ));
            }
            $Views = new View($this, false);
            $overallRecords = array();
            $result = array();
            if ($request['view'] == "followings") {
                $overallRecords =  $this->Follow->getUserFollowings(
                    $this->Auth->user('id')
                );
            } else {
                $overallRecords =  $this->Follow->getUserFollowers(
                    $this->Auth->user('id')
                );
            }
            $currentPage = 1;
            if (isset($request['page'])) {
                $currentPage = $request['page'];
            }
            $maxNumberOfPages = ceil(count($overallRecords) / 20);
            if ($request['page'] > $maxNumberOfPages) {
                $currentPage = $maxNumberOfPages;
            }
            if ($request['page'] <= 0) {
                $currentPage = 1;
            }
            $offset = ($currentPage - 1) * MAX_POSTS_PER_PAGE;
            if ($request['view'] == "followings") {
                if ($request['search'] != "") {
                    $result =  $this->Follow->getUserFollowings(
                        $this->Auth->user('id'),
                        20,
                        0,
                        $request['search']
                    );
                } else {
                    $result =  $this->Follow->getUserFollowings(
                        $this->Auth->user('id'),
                        20,
                        0
                    );
                }
                if (count($result) == 0) {
                    return json_encode(array(
                        "result" => "success",
                        "description" => "The request returned empty.",
                        "html" => $Views->element('Network/no_followings'),
                        "view" => $request['view'],
                        "count" => 0,
                        "pagination" => ""
                    ));
                }
            } else {
                if ($request['search'] != "") {
                    $result =  $this->Follow->getUserFollowers(
                        $this->Auth->user('id'),
                        20,
                        0,
                        $request['search']
                    );
                } else {
                    $result =  $this->Follow->getUserFollowers(
                        $this->Auth->user('id'),
                        20,
                        0
                    );
                }
                if (count($result) == 0) {
                    return json_encode(array(
                        "result" => "success",
                        "description" => "The request returned empty.",
                        "html" => $Views->element('Network/no_followers'), 
                        "view" => $request['view'],
                        "count" => 0,
                        "pagination" => ""
                    ));
                }
            }
            $html = "";
            $pagination = "";
            for ($i = 0; $i < count($result); $i++) {
                $html = $html . $Views->element('Network/user', array(
                    "user" => $result[$i]['User'],
                    "follow" => $result[$i]['Follow']
                ));
            }
            $prevPage = ($currentPage > 1) ? $currentPage - 1 : 1 ;
            $nextPage = ($currentPage < $maxNumberOfPages) ? $currentPage + 1 : $maxNumberOfPages ;
            $pagination = $Views->element('js_pagination', array(
                'function' => 'loadNetwork',
                'maxNumberOfPages' => $maxNumberOfPages,
                'currentPage' => $currentPage,
                'prevPage' => $prevPage ,
                'nextPage' => $nextPage
            ));
            return json_encode(
                array(
                "result" => "success",
                "description" => "The request was successful.",
                "view" => $request['view'],
                "overallCount" => count($overallRecords),
                "count" => count($result),
                "html" => $html,
                "pagination" => $pagination
                )
            );
        }
    }
    /**
    * The profile action that renders the profile
    * view.
    *
    */
    public function profile()
    {
        $this->loadModel('Follow');
        $this->loadModel('Post');
        $this->set(
            'userProfile',
            $this->User->getWithId(
                $this->Auth->user('id')
            )
        );
        $this->set(
            'Followers',
            $this->Follow->getUserFollowers(
                $this->Auth->user('id')
            )
        );
        $this->set(
            'Followings',
            $this->Follow->getUserFollowings(
                $this->Auth->user('id')
            )
        );
        $currentPage = 1;
        $overallPosts = count($this->Post->getUserPosts(
            $this->Auth->user('id')
        ));
        $maxNumberOfPages = ceil($overallPosts / MAX_POSTS_PER_PAGE);
        if (
            isset($this->request->query['page'])
        ) {
            $currentPage = $this->request->query['page'];
            if ($currentPage <= 0) {
                $currentPage = 1;
            }
        }
        if ($currentPage > $maxNumberOfPages) {
            $currentPage = $maxNumberOfPages;
        }
        $offset = ( ($currentPage - 1) * MAX_POSTS_PER_PAGE);
        $posts = $this->Post->getUserPosts(
            $this->Auth->user('id'),
            MAX_POSTS_PER_PAGE,
            $offset
        );
        $prevPage = $currentPage - 1;
        $nextPage = $currentPage + 1;
        if ($prevPage == 0) {
            $prevPage = 1;
        }
        if ($nextPage > $maxNumberOfPages) {
            $nextPage = $maxNumberOfPages;
        }
        $this->set('posts', $posts);
        $this->set(
            'pagination',
            array(
                "numberOfPages" => $maxNumberOfPages,
                "currentPage" => $currentPage,
                "prevPage" => $prevPage,
                "nextPage" => $nextPage
            )
        );
    }
    /**
    * The view action that views other profile.
    * @param int $id The id of the user to view.
    */
    public function view($id)
    {
        $this->loadModel('Follow');
        $this->loadModel('Post');
        $this->set(
            'userProfile',
            $this->User->getWithId(
                $id
            )
        );
        $this->set(
            'Followers',
            $this->Follow->getUserFollowers(
                $id
            )
        );
        $this->set(
            'Followings',
            $this->Follow->getUserFollowings(
                $id
            )
        );
        $currentPage = 1;
        $overallPosts = count($this->Post->getUserPosts(
            $id
        ));
        $maxNumberOfPages = ceil($overallPosts / MAX_POSTS_PER_PAGE);
        if (
            isset($this->request->query['page'])
        ) {
            $currentPage = $this->request->query['page'];
            if ($currentPage <= 0) {
                $currentPage = 1;
            }
        }
        if ($currentPage > $maxNumberOfPages) {
            $currentPage = $maxNumberOfPages;
        }
        $offset = ( ($currentPage - 1) * MAX_POSTS_PER_PAGE);
        $posts = $this->Post->getUserPosts(
            $id,
            MAX_POSTS_PER_PAGE,
            $offset
        );
        $prevPage = $currentPage - 1; 
        $nextPage = $currentPage + 1;
        if ($prevPage == 0) {
            $prevPage = 1;
        }
        if ($nextPage > $maxNumberOfPages) {
            $nextPage = $maxNumberOfPages;
        }
        $this->set('posts', $posts);
        $this->set(
            'pagination',
            array(
                "numberOfPages" => $maxNumberOfPages,
                "currentPage" => $currentPage,
                "prevPage" => $prevPage,
                "nextPage" => $nextPage
            )
        );
    }
    /**
    * Receives an AJAX request to upload new profile picture
    * for the current account. This action receives a $_FILE object
    * or a base64 data of a cropped circular image.
    */
    public function uploadImage()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $request = $this->request->data;
            if (empty($request)) {
                return json_encode(
                    array(
                    'result' => 'failed',
                    'description' => 'No data sent by the client. Uploading aborted.'
                    )
                );
            }
            if (!$this->Auth->user('id')) {
                return json_encode(
                    array(
                        'result' => 'failed',
                        'description' =>
                        'You must be logged in before you can upload images.'
                    )
                );
            }
            $defaultPath = 'user/';
            if ($request['uploadAction'] == "crop_existing") {
                $image_parts = explode(",", $request["fileToUpload"]);
                $image_base64 = base64_decode($image_parts[1]);
                $currentFileType = $this->User->findById(
                    $this->Auth->user('id')
                )['User']['image_file_type'];
                $file = $defaultPath
                . $this->Auth->user('id') . '.' . $currentFileType;
                if (!file_exists($defaultPath)) {
                    mkdir($defaultPath, 0777, true);
                }
                if ($currentFileType != "") {
                    if (
                        file_exists(
                            $defaultPath . $this->Auth->user('id') . '.'  . $currentFileType
                        )
                    ) {
                        unlink($defaultPath . $this->Auth->user('id') . '.' . $currentFileType);
                    }
                }
                file_put_contents($file, $image_base64);
                return json_encode(array(
                    'result' => 'upload_success',
                    'description' => 'The upload was successful'
                ));
            } elseif ($request['uploadAction'] == "new_upload") {
                if (!isset($_FILES['fileToUpload'])) {
                    return "invalid_file_upload";
                }
                if ($_FILES['fileToUpload']["size"] >= 50000000) {
                    return json_encode(array(
                        'result' => 'p_file_too_large',
                        'description' => 'File too large. Upload aborted.'
                    ));
                } 
                $isImage = getimagesize($_FILES['fileToUpload']['tmp_name']) ? true : false;
                if (!$isImage) {
                    return json_encode(array(
                        'result' => 'p_file_not_image',
                        'description' => 'The file that you are uploading is not an image.'
                    ));
                }
                $fileType = pathinfo($_FILES['fileToUpload']["name"], PATHINFO_EXTENSION);
                if (!file_exists($defaultPath)) {
                    mkdir($defaultPath, 0777, true);
                }
                $currentFileType = $this->User->findById(
                    $this->Auth->user('id')
                )['User']['image_file_type'];

                if($currentFileType != ""){
                    if (file_exists($defaultPath . $this->Auth->user('id') . '.' . $currentFileType)) {
                        unlink($defaultPath . $this->Auth->user('id') . '.' . $currentFileType);
                        if (
                            file_exists(
                                $defaultPath . $this->Auth->user('id') . '_original.' . $currentFileType
                            )
                        ) {
                            unlink($defaultPath . $this->Auth->user('id') . '_original.' . $currentFileType);
                        }
                    }
                }
                $new_file_name = $this->Auth->user('id') . "." . $fileType;
                if (
                    move_uploaded_file(
                        $_FILES['fileToUpload']["tmp_name"],
                        $defaultPath . $new_file_name
                    )
                ) {
                    if (
                        !copy(
                            $defaultPath . $new_file_name, 
                            $defaultPath . $this->Auth->user('id') . "_original." . $fileType
                        )
                        ) {
                        return json_encode(array(
                                'result' => 'failed',
                                'description' => 'Failed to back up the original uploaded file.'
                                )
                        );
                    }
                    $this->User->id = $this->Auth->user('id');

                    $imageType = array(
                        'image_file_type' => $fileType
                    );

                    $this->User->save($imageType);

                    return json_encode(array(
                        'result' => 'upload_success',
                        'description' => 'The upload was successful'
                    ));
                }

            } else {
                return json_encode(array(
                    'result' => 'p_upload_failed',
                    'description' => 'The upload action requested by the client is not recognized.'
                ));
            }
        }
    }
}
