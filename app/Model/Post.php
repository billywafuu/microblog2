<?php 
/**
 * Handles logic of all Post related operations.
 * @package       app.Model
 */
class Post extends AppModel
{
    public $belongsTo = array(
        'Owner' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Retweet' => array(
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'conditions' => array(
                'Post.deleted' => 0
            )
        )
    );
    public $hasMany = array(
        'Likes' => array(
            'className' => 'Like',
            'foreignKey' => 'post_id',
            'conditions' => array(
                'Likes.deleted' => 0
            )
        )
    );

    /**
     * @var int Description of property.
     *
     */
    private $maxPostPerPage = 10;

    /**
    *
    * Gets a user row with matching id.
    *
    * @param    string  $user_id The id of the user that you want to get the post of.
    * @param    int     $limit The number of posts that would be retrieved.
    * @param    int     $offset The offset index where the engine will start retrieving.
    * @return   array
    *
    */
    public function getUserPosts($user_id, $limit = null, $offset = null)
    {
        if ($user_id == null) {
            return null;
        }
        if ($limit) {
            $limitRows = $limit;
        }
        if ($offset) {
            $offsetRows = $offset;
        }
        if ($limit === null && $offset === null) {
            return $this->find('all', array(
                'conditions' => array('Post.user_id' => $user_id, 'Post.deleted' => 0),
                "order" => array('Post.created DESC')
            ));
        }
        return $this->find('all', array(
            'conditions' => array('Post.user_id' => $user_id, 'Post.deleted' => 0),
            "limit" => $limit,
            "offset" => $offset,
            "order" => array('Post.created DESC')
        ));
    }
    /**
    *
    * Uploads an image for a post.
    *
    * @param  int  $id The id of the post that the image will be uploaded.
    * @param object $file The file itself.
    * @return bool Returns true if the operation successed otherwise false.
    */
    public function uploadImage($id, $file)
    {
        if (empty($file['name'])) {
            return false;
        }
        if (!getimagesize($file["tmp_name"])) {
            return false;
        }
        if (empty($this->Post->findById($id))) {
            return false;
        }
        $uploadPath = 'post/';
        $imageFileType = strtolower(
            pathinfo(
                $uploadPath . basename($file["name"]),
                PATHINFO_EXTENSION
            )
        );
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif") {
          return false;
        }
        $fileName = $id . "." . $imageFileType;
        $newFile = $uploadPath . $fileName;
        if (file_exists($newFile)) {
            unlink($newFile);
        }
        if (move_uploaded_file($file['tmp_name'],$uploadPath.$fileName)) {
            $this->Post->id = $id;
            $image = array(
                "Post" => array(
                    "image_file_type" => $imageFileType,
                )
            );
            $this->Post->save($image);
            return true;
        } else {
            return false;
        }
    }
    
    /**
    *
    * A function that returns users that matches the search value across all columns.
    *
    * @param string $value The search value to match against the Post table.
    * @param int $limit The limit of records to return.
    * @param int $offset The index where to start.
    * @return array The overall count, the returned rows as array, number of pages
    * and the offset.
    */
    public function search($value, $limit, $offset) {
        $overallCount = count(
            $this->find('all', array(
                "conditions" => array(
                    'OR' => array(
                        array('Post.title LIKE' => '%'.$value.'%'),
                        array('Post.body LIKE' => '%'.$value.'%')
                    ),
                    "Post.deleted" => 0
                )
            ))
        );
        $returnRows = $this->find('all', array(
            "conditions" => array(
                'OR' => array(
                    array('Post.title LIKE' => '%'.$value.'%'),
                    array('Post.body LIKE' => '%'.$value.'%')
                ),
                "Post.deleted" => 0
            ),
            "limit" => $limit, 
            "offset"=> $offset)
        );
        if($overallCount == 0){
            // Create the return. 
            $result = array(
                "model" => "Post",
                "overallCount" => $overallCount, 
                "returnRows" => $returnRows,
                "pages" => 0,
                "offset" => $offset
            );
            return $result;
        }
        $result = array(
            "model" => "Post",
            "overallCount" => $overallCount, 
            "returnRows" => $returnRows,
            "pages" => ceil($overallCount / count($returnRows)),
            "offset" => $offset
        );
        return $result;
    }
}