<?= $this->element('Posts/retweeter') ?>
<div class="container-fluid p-0">
    <div class="card border-0 microblog-post">
            <div class="card-body" id="post-container">
                <?php 
                    if(empty($posts)){
                        echo $this->element('Posts/no_post');
                    }else{
                        echo $this->element('Posts/pagination', $pagination);
                    }
                ?>    
                <?php 
                    foreach($posts as $post){
                        echo $this->element('Posts/post', array(
                            "post" => $post['Post'],
                            "owner"=> $post['Owner'],
                            "likes"=> $post['Likes'],
                            "retweet"=> $post['Retweet']));
                        }   
                ?>
            </div>
    </div>
        <?php 
            if(!empty($posts)){
                echo $this->element('Posts/pagination', $pagination);
            }
        ?>  
</div>
