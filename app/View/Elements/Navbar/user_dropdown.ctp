<div class="d-flex">

	<div class="p-1">
		<a href="<?= $this->base ?>/profile">
			<img src="<?= $this->webroot ?>user/<?= $currentUser['User']['id'] ?>.<?= $currentUser['User']['image_file_type'] ?>" class="rounded-circle" 
			style="width:30px; height: 30px">
		</a>
	</div>

	<div>
		<div class="dropdown">
			<button class="btn btn-transparent dropdown-toggle text-light mr-4" 
				type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<strong><?= $currentUser['User']['first_name'] ?></strong>
			</button>
			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				<?php echo $this->Html->link( "My Profile",
					array('controller' => 'users', 'action' => 'profile'),
					array('class' => 'dropdown-item') ); 
				?>
				<?php echo $this->Html->link( "Settings",
					array('controller' => 'users', 'action' => 'settings'),
					array('class' => 'dropdown-item') ); 
				?>
				<?php echo $this->Html->link( "Change Password",
					array('controller' => 'users', 'action' => 'changepassword'),
					array('class' => 'dropdown-item') ); 
				?>
				<?php echo $this->Html->link( "My Network",
					array('controller' => 'users', 'action' => 'network'),
					array('class' => 'dropdown-item') ); 
				?>
				<?php echo $this->Html->link( "Log out",
					array('controller' => 'microblog', 'action' => 'logout'),
					array('class' => 'dropdown-item') ); 
				?>
			</div>
		</div>
	</div>
</div>
