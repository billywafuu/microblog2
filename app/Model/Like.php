<?php 
/**
 * Handles logic that has something to do with Likes of post.
 *
 * @package       app.Model
 */
class Like extends AppModel
{
    /**
    *
    * Gets the like record from the likes table.
    *
    * @param int $post_id The id of the post to be retrieved.
    * @param int $user_id The id of the user to be retrieved.
    * @return array Returns an array of Like if there is a record else null.
    *
    */
    public function getLike($user_id, $post_id)
    {
        return $this->find('first', array(
            'conditions' => array(
                'Like.post_id' => $post_id,
                'Like.user_id' => $user_id,
                'Like.deleted' => 0
            )
        ));
    }
    /**
    *
    * Gets the like record from the likes table.
    *
    * @param int $post_id The id of the post to retrieve the likes of.
    * @return array Returns an array of users that liked the post.
    *
    */
    public function getLikes($post_id)
    {
        return $this->find('all', array(
            'conditions' => array(
                'Like.post_id' => $post_id,
                'Like.deleted' => 0
            )
        ));
    }
    /**
    *
    * Gets the boolean value if the user_id has liked the post_id.
    *
    * @param int $user_id The id of the user to check.
    * @param int $post_id The id of the post.
    * @return bool Returns a bool if its liked or not.
    *
    */
    public function hasUserLikedPost($user_id, $post_id)
    {
        if (empty($this->getLike($user_id, $post_id))) {
            return false;
        }
        return true;
    }
    /**
    *
    * Gets the number of likes of a certain post.
    *
    * @param int $id The id of the post.
    * @return int The number of likes.
    *
    */
    public function getNumberOfLikes($post_id)
    {
        return count($this->find('all', array(
            "conditions" => array(
                "deleted" => 0,
                "deleted_at" => null,
                "post_id" => $post_id
            )
        )));
    }
}
