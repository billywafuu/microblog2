<?php 
App::uses('AppModel', 'Model');
App::uses('CakeEmail', 'Network/Email');
/**
 * Handles account activation logic.
 *
 * The activation model handles the email sending
 * and activating of new accounts.
 * @package       app.Model
 */
class Activation extends AppModel
{
    /**
     * Sends activation email to user email address.
     *
     * Sends activation email to user's email address.
     * This function receives the user_id, the id of new activation code
     * and the activation token to validate the activation record.
     * 
     * The id and the activation token is manually inputted.
     *
     * @param int $user_id The user_id of the user that will receive the activation email.
     * @param string $id The id of the activation record.
     * @param string $activation_token The token of the combination.
     * @return bool Returns true on success otherwise false.
     */
    public function send($user_id, $id, $activation_token)
    {
        $userModel = ClassRegistry::init('User');
        $user = $userModel->getWithId($user_id);
        if (empty($user)) {
            return false;
        }
        if (empty($user['User']['email_address'])) {
            return false;
        }
        $Email = new CakeEmail();
        $Email->config('smtp');
        $activation = array(
            "user_id" => $user_id,
            "id"      => $id, 
            "activation_token" => $activation_token
        );
        $this->save($activation);
        $Email->template('activation')
            ->viewVars(array(
                'id' => $id,
                'activation_token' => $activation_token,
                'first_name' => $user['User']['first_name']
            ))
            ->emailFormat('html')
            ->to($user['User']['email_address'])
            ->subject('Microblog Registration')
            ->send();
        return true;
    }
    /**
     *
     * Activates a user account using the id and the activation_token
     * If the id + token combination did not match, it wont activate the user account.
     * The id and the activation token is manually inputted.
     *
     * @param string $id The id of the activation record.
     * @param string $activation_token The token of the combination.
     * @return bool Returns true on success otherwise false.
     */
    public function activate($id, $activation_token)
    {
        $activation = $this->find('first', array(
            'conditions' => array('Activation.id' => $id, 
            'Activation.activation_token' => $activation_token)
        ));
        if (empty($activation)) {
            return false;
        }
        $userModel = ClassRegistry::init('User');
        $followModel = ClassRegistry::init('Follow');
        $user = $userModel->getWithId($activation['Activation']['user_id']);
        if (empty($user)) {
            return false;
        }
        if (!file_exists('user/')) {
            mkdir('user/', 0777, true);
        }
        if (!copy('img/default-profile.png', 'user/' . $user['User']['id'] . "_original.png")) {
            return false;
        }
        if (!copy('img/default-profile.png', 'user/' . $user['User']['id'] . ".png")) {
                return false;
        }
        $followModel->followUser($user['User']['id'], $user['User']['id']);
        $userModel->id = $user['User']['id'];
        $userModel->save(array('status' => 1));
        $this->delete($id);
        $userModel->deleteAll(
            array(
            'User.status' => 0,
            'User.email_address' => $user['User']['email_address']), false
        );
        return true;
    }
}
