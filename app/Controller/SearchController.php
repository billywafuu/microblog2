<?php
/**
 * The controller that manages Searching.
 *
 * @package app.Controller
 */
class SearchController extends AppController {
    public function beforeFilter()
    {
        parent::beforeFilter();
    }
    /**
    * The main index of the Search controller.
    *
    */
    public function index()
    {
        $this->loadModel('User');
        $this->loadModel('Post');
        if ($this->request->is('get')) {
            if (isset($this->request->query['q'])) {
                if ($this->request->query['q'] == "") {
                    $this->set('result', array());
                    return;
                }
                $this->Session->write('Search.q', $this->request->query['q']);
                $requestPage = 1;
                if (isset($this->request->query['page'])) {
                    $requestPage = $this->request->query['page'];
                }
                $offset = 0;
                if ($requestPage > 1) {
                    $offset = (10 * ($requestPage - 1));
                }
                $result = array();
                if (isset($this->request->query['target'])) {
                    if ($this->request->query['target'] == "users") {
                        $result = $this->User->search($this->request->query['q'], 10, $offset);
                    } elseif ($this->request->query['target'] == "posts") {
                        $result = $this->Post->search($this->request->query['q'], 10, $offset);
                    } else {
                        $result = $this->User->search($this->request->query['q'], 10, $offset);
                    }
                } else {
                    $result = $this->User->search($this->request->query['q'], 10, $offset);
                }
                $this->set('result', $result);
            }
        }
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $Views = new View($this, false);
            $searchRequest = $this->request->data;
            if (!$this->Auth->user('id')) {
                return json_encode(array(
                    "result" => "unauthorized_request",
                    "description" => "You need to login before you can search."
                ));
            }
            if (empty($searchRequest)) {
                return json_encode(array(
                    "result" => "failed",
                    "description" => "No search request received."
                ));
            }
            $requestPage = 1;
            if ($searchRequest['searchQuery'] == "") {
                return json_encode(array(
                    "result" => "success",
                    "html"   => $Views->element('Search/search_guide'),
                    "overallCount" => 0,
                    "pages" => 0,
                    "currentPage" => 1,
                    "currentOffset" => 0
                ));
            }
            if (isset($searchRequest['searchPage'])) {
                $requestPage = $searchRequest['searchPage'];
            }
            if ($requestPage < 1) {
                $requestPage = 1;
            }
            $offset = 0; 
            if ($requestPage > 1) {
                $offset = (10 * ($requestPage - 1));
            }
            $result = array();
            if (isset($searchRequest['searchTarget'])) {
                if ($searchRequest['searchTarget'] == "User") {
                    $result = $this->User->search($searchRequest['searchQuery'], 10, $offset);
                } elseif ($searchRequest['searchTarget'] == "Post") {
                    $result = $this->Post->search($searchRequest['searchQuery'], 10, $offset);
                } else {
                    $result = $this->User->search($searchRequest['searchQuery'], 10, $offset);
                }
            } else {
                $result = $this->User->search($searchRequest['searchQuery'], 10, $offset);
            }
            if ($result['overallCount'] == 0) {
                return json_encode(array(
                    "result" => "success",
                    "html"   => $Views->element('Search/search_empty'), 
                    "overallCount" => 0,
                    "pages" => 0,
                    "currentPage" => 1,
                    "currentOffset" => 0
                ));
            }
            $html = "";
            $pagination = "";
            if ($result['model'] == 'User') {
                foreach ($result['returnRows'] as $user) {
                    $html = $html . $Views->element('Search/user_result', array('user' => $user['User']));
                }
            } elseif ($result['model'] == 'Post') {
                foreach ($result['returnRows'] as $post) {
                    $html = $html . $Views->element('Search/post_result', array('post' => $post['Post']));
                }
            } else {
                foreach ($result['returnRows'] as $user) {
                    $html = $html . $Views->element('Search/user_result', array('user' => $user['User']));
                }
            }
            $this->Session->write('Search.target', $searchRequest['searchTarget']);
            return json_encode(array(
                "result" => "success",
                "html"   => $html, 
                "overallCount" => $result['overallCount'],
                "pages" => $result['pages'], 
                "currentPage" => $requestPage, 
                "currentOffset" => $offset
            ));
        }
    }
}