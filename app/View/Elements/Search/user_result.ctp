<div class="container-fluid">
<div class="row">
    <div class="col-md-8">
        <div class="d-flex flex-row">
            <div class="p-2">
                <img style="width:50px; height:50px" src="<?= $this->webroot ?>user/<?= $user['id'] ?>.<?= $user['image_file_type'] ?>" class="rounded-circle" alt="">
            </div>
            <div class="p-2">
                <h4><?= $user['first_name'] ?> <?= $user['last_name'] ?></h4>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="d-flex flex-row flex-row-reverse">
            <div class="p-2">
                <a href="<?= $this->base ?>/users/view/<?= $user['id'] ?>" class="badge badge-primary"><i class="fas fa-user"></i> See profile</a>
            </div>
        </div>
    </div>
</div>
    <hr>
</div>