<div id="flash-<?php echo h($key) ?>" class="alert alert-warning">
<i class="fas fa-exclamation-circle"></i> <?php echo h($message) ?>
</div>