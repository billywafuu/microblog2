<div class="container-fluid text-center p-4" id="search-empty">
    <h4><i class="fas fa-search"></i> No results found</h4> 
    <small class="text-muted">Try changing your search keywords.</small>
</div>