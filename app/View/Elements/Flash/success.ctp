<div id="flash-<?php echo h($key) ?>" class="alert alert-success alert-dismissible fade show" role="alert">
    <i class="far fa-check-circle"></i> <?php echo h($message) ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>