<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<link rel="apple-touch-icon" sizes="57x57" href="<?= $this->webroot ?>/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= $this->webroot ?>/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= $this->webroot ?>/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= $this->webroot ?>/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= $this->webroot ?>/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= $this->webroot ?>/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= $this->webroot ?>/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= $this->webroot ?>/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= $this->webroot ?>/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?= $this->webroot ?>/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= $this->webroot ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= $this->webroot ?>/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= $this->webroot ?>/favicon-16x16.png">
	<link rel="manifest" href="<?= $this->webroot ?>/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= $this->webroot ?>/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<?php
		echo $this->Html->meta('icon');	
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" 
        crossorigin="anonymous"
    >
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
	
	<?php if(AuthComponent::user('id')):?>
		<script src="<?= $this->webroot ?>js/post.js"></script>
		<script src="<?= $this->webroot ?>js/comment.js"></script>
		<script src="<?= $this->webroot ?>js/like.js"></script>
		<script src="<?= $this->webroot ?>js/follow.js"></script>
		<script src="<?= $this->webroot ?>js/debounce.min.js"></script>
	<?php endif;?>
	<?= $this->Html->css('bootstrap.min.css') ?>
	<?= $this->Html->css('microblog.css') ?>
	<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
</head>
<body>
	<div id="container">
		<div id="header">
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary" id="main-navbar">
			<a class="navbar-brand" href="http://<?=$_SERVER['SERVER_NAME'] ?><?= $this->base?>">
				<img src="<?= $this->webroot ?>img/logo.jpg" width="30" height="30" class="d-inline-block align-top" alt="">
  			</a>
			<?php if(AuthComponent::user('id')):?>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<form class="form-inline my-2 my-lg-0" action="<?= $this->base ?>/search">
					<div class="input-group">
						<input type="text" class="form-control" name="q" id="searchQuery" placeholder="Search Microblog" 
						value="">
						<div class="input-group-append">
							<button class="btn btn-success" type="submit"><i class="fas fa-search"></i></button>
						</div>
					</div>
					</form>
					<div class="mobile-only">
						<?php echo $this->element('Navbar/user_dropdown')?>
					</div>
				</div>
				<div class="d-none d-lg-block mr-4">
					<a type="button" href="http://<?=$_SERVER['SERVER_NAME'] ?><?= $this->base?>" class="btn btn-lg btn-primary mr-2"> 
						<i class="fas fa-home"></i>
					</a>
				</div>
				<div class="d-none d-lg-block mr-4">
					<a type="button" href="<?= $this->base ?>/posts/create" class="btn btn-md btn-success mr-2"> 
						<i class="fas fa-plus"></i> Post
					</a>
				</div>
				<div class="d-none d-lg-block mr-4">
					<?php echo $this->element('Navbar/user_dropdown')?>
				</div>
			<?php endif;?>
		</nav>	
		</div>
		<div id="content">
		<?= $this->Flash->render('success') ?>
		<?= $this->Flash->render('error') ?>		
		<?= $this->Flash->render('warning') ?>	
		<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">

		</div>
	</div>
	<script>
		function g(id){ return document.getElementById(id) }
		function bu(){return '<?= $this->base ?>/'}
		$(document).ready(() => {
			var mainNavbar = $("#main-navbar");
			$(document).scroll($.debounce(500, function(e) {
				if(window.pageYOffset > 300){
					$(mainNavbar).addClass("fixed-top");
				}else{ 
					$(mainNavbar).removeClass("fixed-top");
				}
			}));    
			console.log("Welcome to " + bu() + ".");
		})
	</script>
</body>
</html>
