<div class="d-flex justify-content-center pa-4">
    <div class="text-center">
        <?php if(AuthComponent::user('id') == $userProfile['User']['id']):?>
            <h5>You haven't published any post yet</h5>
            <p class="text-muted">Create a post and sharing it to your followers is a great way for them to be up to date with you.</p>        
        <?php else:?>
            <h5>No posts to view</h5>
            <p class="text-muted">We cannot find posts on <?= $userProfile['User']['first_name'] ?>'s profile yet.</p> 
        <?php endif;?>
        <hr>
    </div>
</div>