<?php
/**
 * Handles comments logic.
 *
 * The comments model handles anything business logic
 * that include comments.
 *
 * @package app.Model
 */
class Comment extends AppModel {
    /**
     * @var array Creates a virtual field named timestamp that stores UNIX_TIMESTAMP
     * version of the Comment.created column.
     *
     */
    public $virtualFields = array(
        'timestamp' => 'UNIX_TIMESTAMP(Comment.created)'
    );
    /**
     * Gets comments of a certain post.
     *
     * Gets comments of a certain post and allows the developer
     * to utilized the limit and the offset params for paging.
     *
     * @param int $post_id The post to retrieve the comments of.
     * @param int $limit The limit of comments to retrieve.
     * @param int $offset The index where the engine will start getting comments.
     * @return array An array of comments.
     */
    public function getComments($post_id, $limit = null, $offset = null) {
        if ($limit == null && $offset == null) {
            return $this->find('all', array(
                "conditions" => array(
                    "Comment.post_id" => $post_id,
                    "Comment.deleted" => 0
                ),
                "joins" => array(
                    array(
                        "table" => "users",
                        "alias" => "User",
                        "type"  => "INNER",
                        "conditions" => array(
                            "Comment.user_id = User.id"
                        )
                    )
                ),
                "fields" => array(
                    "Comment.*",
                    "User.id",
                    "User.first_name",
                    "User.last_name",
                    "User.image_file_type"
                ),
                "order" => array('Comment.created DESC')
            ));
        } else {
            return $this->find('all', array(
                "conditions" => array(
                    "Comment.post_id" => $post_id, 
                    "Comment.deleted" => 0
                ),
                "joins" => array(
                    array(
                        "table" => "users",
                        "alias" => "User",
                        "type"  => "INNER",
                        "conditions" => array(
                            "Comment.user_id = User.id"
                        )
                    )
                ),
                "fields" => array(
                    "Comment.*",
                    "User.id",
                    "User.first_name",
                    "User.last_name",
                    "User.image_file_type"
                ),
                "limit" => $limit, 
                "offset" => $offset,
                "order" => array('Comment.created DESC')
            ));
        }
    }
    /**
     * Gets the latest comment from a post.
     *
     * Gets the latest comment from a post
     * given a certain timestamp.
     *
     * @param int $post_id The post to retrieve the latest comment of.
     * @param int $timestamp The timestamp to compare to.
     * @return array An array of latest comment.
     */
    public function getLatest($post_id, $timestamp)
    {
        return $this->find('all', array(
            "conditions" => array(
                "Comment.post_id" => $post_id,
                "Comment.deleted" => 0,
                "Comment.timestamp >" => $timestamp
            ),
            "joins" => array(
                array(
                    "table" => "users",
                    "alias" => "User",
                    "type"  => "INNER",
                    "conditions" => array(
                        "Comment.user_id = User.id"
                    )
                )
            ),
            "fields" => array(
                "Comment.*",
                "User.id",
                "User.first_name",
                "User.last_name",
                "User.image_file_type"
            ),
            "limit" => 5,
            "order" => array('Comment.created DESC')
        ));
    }
}
