
<div class="card my-4 mx-auto" style="width:450px">
    <div class="card-header">
        <h5>Login</h5>
    </div>
    <div class="card-body">
        <form method="POST" action="<?php Router::url('/', true); ?>login">
            <div class="form-group">
                <label for="inputEmailAddress">Email Address</label>
                <input type="email" class="form-control" name="email_address" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label for="inputPassword">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
            <?php echo $this->Html->link( "Forgot your password?",
					array('controller' => '/', 'action' => 'forgotpassword'),
					array('class' => 'dropdown-item text-primary') ); 
				?>
                   
            <div class="text-right">
                <a href="<?php Router::url('/', true); ?>register" class="btn btn-success">Create Account</a>
                <button type="submit" class="btn btn-primary">Login</button>
            </div>
        </form>    

    </div>
</div>
