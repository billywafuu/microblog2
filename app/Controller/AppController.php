<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $components = array(
        'DebugKit.Toolbar',
        'Auth' => array(
            'loginAction' => array(
                'controller' => '/',
                'action' => 'login'
            ),
            'logoutAction' => array(
                'controller' => '/',
                'action' => 'index'
            ),
            'authError' => 'You need to be logged in to access that content.',
            'authenticate' => array(
                'Form' => array(
                    'fields' => array(
                      'username' => 'email_address', //Default is 'username' in the userModel
                      'password' => 'password',  //Default is 'password' in the userModel,
                      'passwordHasher' => 'Blowfish'
                    )
                )
            )
        ),
        'Flash',
        'Session'
    );
    public function beforeFilter()
    {
        if ($this->Auth->user('id')) {
            $this->set(
                'currentUser',
                array("User" => $this->Auth->user())
            );
        }
    }
    public function beforeRender()
    {
        $this->response->header('Cache-Control', 'max-age=0, must-revalidate');
    }
}
