<div class="container-fluid">
    <div class="d-flex flex-row">
        <div class="p-2">
        <a href="<?= $this->base ?>/users/view/<?= $owner['id'] ?>">
            <img style="width:50px; height:50px" src="<?= $this->webroot ?>user/<?= $owner['id'] ?>.<?= $owner['image_file_type'] ?>" class="rounded-circle" alt="">
        </a>
        </div>
        <div class="p-2">
            <h4>
                <a id="microblog-link" href="<?= $this->base ?>/users/view/<?= $owner['id'] ?>">
                    <?= $owner['first_name'] ?> <?= $owner['last_name'] ?>
                </a>
            </h4>

        </div>
    </div>
    <hr>
</div>