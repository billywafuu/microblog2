<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/',           array('controller' => 'microblog', 'action' => 'index', 'home'));
	Router::connect('/login',      array('controller' => 'microblog', 'action' => 'login'));
	Router::connect('/logout',     array('controller' => 'microblog', 'action' =>'logout'));
	Router::connect('/register',   array('controller' => 'microblog', 'action' => 'register'));
	Router::connect('/activation', array('controller' => 'microblog', 'action' => 'activation'));
	Router::connect('/forgotpassword', array('controller' => 'microblog', 'action' => 'forgotpassword'));
	Router::connect('/resetpassword', array('controller' => 'microblog', 'action' => 'forgotpassword'));
	Router::connect('/resetpassword/:id/:reset_token', array('controller' => 'microblog', 'action' => 'resetpassword'),
	array(
		'pass' => array('id','reset_token'),
		'id' => '[a-zA-Z0-9]+',
		'reset_token' => '[a-zA-Z0-9]+'
	)
	);
	Router::connect('/activation/:id/:activation_token', array('controller' => 'microblog', 'action' => 'activation'),
		array(
			'pass' => array('id','activation_token'),
			'id' => '[a-zA-Z0-9]+',
			'activation_token' => '[a-zA-Z0-9]+'
		)
	);
	Router::connect('/profile', array('controller' => 'users', 'action' => 'profile'));
	Router::connect('/:id', array('controller' => 'users', 'action' => 'view'), 
	array(
		'pass' => array('id'),
		'id' => '[0-9]+'
	)
	);

	Router::connect('/users/:id', array('controller' => 'users', 'action' => 'view'), 
		array(
			'pass' => array('id'),
			'id' => '[0-9]+'
		)
	);

	Router::connect('/users/view/:id', array('controller' => 'users', 'action' => 'view'), 
		array(
			'pass' => array('id'),
			'id' => '[0-9]+'
		)
	);

/* Comments */ 
Router::connect('/comments/post', array('controller' => 'comments', 'action' => 'post'));

/* Profile Settings */ 
Router::connect('/profile/settings', array('controller' => 'users', 'action' => 'settings'));

/* Change Passwords */ 
Router::connect('/profile/changepassword', array('controller' => 'users', 'action' => 'changepassword'));

/* Network */ 
Router::connect('/profile/network', array('controller' => 'microblog', 'action' => 'network'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
