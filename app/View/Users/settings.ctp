<?= $this->element('croppie') ?>
<?php echo $this->Flash->render('save_success') ?>
<div class="container">
    <div class="card mt-2 mx-auto microblog-view" id="settingsCard">
        <div class="card-body">
        <h5><?= $this->element('back_arrow')?> Settings</h5>
            <div class="container-fluid mb-3 text-center">
                <div style="width:150px; height:150px" class="mx-auto">
                    <img src="<?= $this->webroot ?>user/<?= $currentUser['User']['id'] ?>.<?= $currentUser['User']['image_file_type'] ?>" class="img-fluid rounded-circle">
                </div>
                <button class="btn btn-sm btn-light mt-4" onclick="$('#photo_upload').modal('show')">
                    <i class="fas fa-upload"></i> Change Image
                </button>
            </div>

            <div class="container-fluid">
                <form method="POST" id="settingsForm">
                    <input type="hidden" name="id" 
                    value="<?= ($currentUser['User']['id']) ? $currentUser['User']['id']  : '' ; ?>">
                    <div class="mb-2">
                        <input class="form-control" type="email" id="inputEmailAddress" name="email_address" 
                        value="<?= ($currentUser['User']['email_address']) ? $currentUser['User']['email_address']  : '' ; ?>">

                        <?php if(isset($errors['email_address'])):?>
                            <small class="text-danger"><?=  h($errors['email_address'][0]) ?></small>
                        <?php else: ?>
                            <small class="text-muted">Your email address.</small>
                        <?php endif ?>                
                    </div>
                    <div class="mb-2">
                        <input class="form-control" type="text"  name="first_name" 
                        value="<?= ($currentUser['User']['first_name']) ? $currentUser['User']['first_name']  : '' ; ?>">
                        <?php if(isset($errors['first_name'])):?>
                            <small class="text-danger"><?=  h($errors['first_name'][0]) ?></small>
                        <?php else: ?>
                            <small class="text-muted">Your first name.</small>
                        <?php endif ?>                   
                    </div>
                    <div class="mb-2">
                        <input class="form-control" type="text"  name="middle_name" 
                        value="<?= ($currentUser['User']['middle_name']) ? $currentUser['User']['middle_name']  : '' ; ?>">
                        <small class="text-muted">Change your middle name.</small>     
                        <?php if(isset($errors['middle_name'])):?>
                            <small class="text-danger"><?=  h($errors['middle_name'][0]) ?></small>
                        <?php else: ?>
                            <small class="text-muted">Your middle name (optional).</small>
                        <?php endif ?>                  
                    </div>
                    <div class="mb-2">
                        <input class="form-control" type="text"  name="last_name" 
                        value="<?= ($currentUser['User']['last_name']) ? $currentUser['User']['last_name']  : '' ; ?>">
                        <small class="text-muted">Change your last name.</small>  
                        <?php if(isset($errors['last_name'])):?>
                            <small class="text-danger"><?=  h($errors['last_name'][0]) ?></small>
                        <?php else: ?>
                            <small class="text-muted">Your last name.</small>
                        <?php endif ?>                      
                    </div>
                    <div class="mb-2">
                        <input class="form-control" type="date"  name="birthdate" 
                        value="<?= ($currentUser['User']['birthdate']) ? $currentUser['User']['birthdate']  : '' ; ?>">
                        <?php if(isset($errors['birthdate'])):?>
                            <small class="text-danger"><?=  h($errors['birthdate'][0]) ?></small>
                        <?php else: ?>
                            <small class="text-muted">Your birth date.</small>
                        <?php endif ?>                     
                    </div>
                    <div class="text-right">
                        <button type="button" onclick="saveForm()" class="btn btn-primary"><i class="fas fa-save"></i> Save Changes</button>
                    </div>
                </form>           
            </div>

        </div>
    </div>
</div>

<script> 
    var currentEmailAddress = '<?= ($currentUser['User']['email_address']) ? h($currentUser['User']['email_address'])  : '' ; ?>';
    function saveForm(){
        if(currentEmailAddress != "" && currentEmailAddress != g('inputEmailAddress').value){
            $.confirm(
                {
                    title: 'Change Email Address?',
                    content: 'When you change your email address for this account, you need to activate it again.' +
                    '<br><br><small class="text-muted">We will be sending a new activation link to the new email address that you entered.</small><br><hr>Continue?',
                    buttons: { 
                        confirm: { 
                            text: 'Yes',
                            btnClass: 'btn-success',
                            action: () => {
                                g('settingsForm').submit();
                            }
                        },
                        cancel: {
                            text: 'No'
                        }
                    }
                }
            );
        }else{
            g('settingsForm').submit();
        }
       
    }
</script>