<div class="card bg-light border-0 my-1" id="comment-<?= $comment['id'] ?>">
    <div class="card-body">
    <?php if($comment['user_id'] == AuthComponent::user('id')):?>
        <button type="button" class="close" onclick="deleteComment('<?= $comment['id'] ?>')">
            <span aria-hidden="true">&times;</span>
        </button>
    <?php endif ?>
        <div class="d-flex flex-column">
            <div class="d-flex flex-row">
                <div class="p-1">
                    <a href="<?= $this->base ?>/users/view/<?= $user['id'] ?>">
                        <img style="width:40px; height:40px" src="<?= $this->webroot ?>user/<?= $user['id'] ?>.<?= $user['image_file_type'] ?>" class="rounded-circle" alt="">
                    </a>
                </div>
                <div class="p-1">
                    <h6>
                        <a id="microblog-link" href="<?= $this->base ?>/users/view/<?= $user['id'] ?>">
                            <?= $user['first_name'] ?> <?= $user['last_name'] ?>
                        </a>
                    </h6>
                    <div>
                        <small class="text-muted"><?= $comment['comment'] ?></small>
                    </div>             
                </div>
            </div>
        </div>
    </div>
</div>