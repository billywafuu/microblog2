<?php 
App::uses('AppModel', 'Model');
/**
 * Handles User logics.
 * @package       app.Model
 */
class User extends AppModel
{
    /**
     * @var array The user model's validation rules.
     *
     */
    public $validate = array(
        'first_name' => array(
            'rule' => '/[一-龠]+|[ぁ-ゔ]+|[ァ-ヴー]+|[a-zA-Z0-9]+|[ａ-ｚＡ-Ｚ０-９]+|[々〆〤]+/u',
            'message' => 'Only letters are allowed on names and cannot be empty.',
            'allowEmpty' => false),
        'middle_name' => array(
            'rule' => '/[一-龠]+|[ぁ-ゔ]+|[ァ-ヴー]+|[a-zA-Z0-9]+|[ａ-ｚＡ-Ｚ０-９]+|[々〆〤]+/u',
            'message' => 'Only letters are allowed on names.',
            'allowEmpty' => true),
        'last_name' => array(
            'rule' => '/[一-龠]+|[ぁ-ゔ]+|[ァ-ヴー]+|[a-zA-Z0-9]+|[ａ-ｚＡ-Ｚ０-９]+|[々〆〤]+/u',
            'message' => 'Only letters are allowed on names and cannot be empty.',
            'allowEmpty' => false),
        'email_address' => array(
            'rule' => 'email',
            'message' => 'Please enter a valid email address.',
            'allowEmpty' => false),
        'birthdate' => array(
            'rule' => 'date',
            'message' => 'Please enter a valid date.',
            'allowEmpty' => false),
        'password' => array(
            'rule' => array('minLength', 8),
            'message' => 'Password requires minimum length of 8
            for maximum security.'
        )
    );
    /**
    *
    * Gets a user row with matching email address.
    *
    * @param string $email_address The email address as string.
    * @return array An array of user records.
    *
    */
    public function getWithEmailAddress($email_address)
    {
        return $this->find('first', array(
            'conditions' => array('User.email_address' => $email_address)
        ));
    }
    /**
    *
    * Checks if the email address is available for use.
    *
    * @param string $email_address The email address as string.
    * @return bool Returns true if email address is available. False otherwise.
    *
    */
    public function isEmailAddressAvailable($email_address)
    {
        $email_address = $this->find(
            'first',
            array(
                'conditions' => array(
                    'User.email_address' => $email_address,
                    'User.status' => 1,
                )
            )
        );
        if (empty($email_address)) {
            return true;
        }
        return false;
    }
    /**
    *
    * Gets user row with matching id.
    *
    * @param int $id The id of the user.
    * @return array An array of user records.
    *
    */
    public function getWithId($id)
    {
        return $this->find('first', array(
            'conditions' => array('User.id' => $id)
        ));
    }
    /**
    *
    * A function that returns users that matches the search value across all columns.
    *
    * @param string $value The search value to match against the User table.
    * @param int $limit The limit of records to return.
    * @param int $offset The index where to start searching.
    * @return array The overall count, the returned rows as array, number of pages
    * and the offset.
    */
    public function search($value, $limit, $offset)
    {
        $overallCount = count(
            $this->find('all', array(
                "conditions" => array(
                    'OR' => array(
                        array('CONCAT(User.last_name, " ", User.first_name) LIKE'
                        => '%' . $value . '%'),
                        array('CONCAT(User.first_name, " ", User.last_name) LIKE'
                        => '%' . $value . '%'),
                        array('
                        CONCAT(
                        User.first_name, " ", User.middle_name, " ", User.last_name
                        ) LIKE' 
                        => '%' . $value . '%'),
                        array('CONCAT(User.last_name, " ", User.first_name, " ", User.middle_name) LIKE' 
                        => '%' . $value . '%'),
                        array('User.email_address LIKE' => '%' . $value . '%')
                    ),
                    "User.status" => 1
                )
            ))
        );
        $returnRows = $this->find('all', array(
            "conditions" => array(
                'OR' => array(
                    array('CONCAT(User.last_name, " ", User.first_name) LIKE' => '%' . $value . '%'),
                    array('CONCAT(User.first_name, " ", User.last_name) LIKE' => '%' . $value . '%'),
                    array('CONCAT(User.first_name, " ", User.middle_name, " ", User.last_name) LIKE'
                    => '%' . $value . '%'),
                    array('CONCAT(User.last_name, " ", User.first_name, " ", User.middle_name) LIKE'
                    => '%' . $value . '%'),
                    array('User.email_address LIKE' => '%' . $value . '%')
                ),
                "User.status" => 1
            ),
            "limit"  => $limit,
            "offset" => $offset
        ));
        if ($overallCount == 0) {
            $result = array(
                "model" => "User",
                "overallCount" => $overallCount,
                "returnRows" => $returnRows,
                "pages" => 0,
                "offset" => $offset
            );
            return $result;
        }
        $result = array(
            "model" => "User",
            "overallCount" => $overallCount, 
            "returnRows" => $returnRows,
            "pages" => ceil($overallCount / count($returnRows)),
            "offset" => $offset
        );
        return $result;
    }
}
