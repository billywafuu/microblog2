<?= $this->element('croppie') ?>
<div class="container">
    <div class="card microblog-view">
        <div class="card-body">
            <div class="d-flex flex-row">
                <div class="mr-4">
                    <a href="#" onclick="$('#photo_upload').modal('show')">
                        <img src="<?= $this->webroot ?>user/<?= $userProfile['User']['id'] ?>.<?= $userProfile['User']['image_file_type'] ?>" 
                        class="rounded-circle" 
                        style="width:100px; height: 100px">
                    </a>
                    <br>
                </div>
                <div>
                    <div class="row">
                        <div class="col-12">
                            <h3><?= $userProfile['User']['first_name']?> <?= $userProfile['User']['last_name'] ?></h3>
                            <hr>
                        </div>
                        <div class="col-md-6">
                            <h4><?= count($Followers) ?></h4>
                            <h6>Followers</h6>
                        </div>
                        <div class="col-md-6">
                            <h4><?= count($Followings) ?></h4>
                            <h6>Following</h6>
                        </div>
                        <div class="col-12 text-right">
                        <?php if(AuthComponent::user('id') != $userProfile['User']['id']):?>
                            <?php
                                $isFollowing = false; 
                                for($i = 0; $i < count($Followers); $i++)
                                {
                                    if ($Followers[$i]['User']['id'] == AuthComponent::user('id')) {
                                        $isFollowing = !$isFollowing;
                                        break;
                                    }
                                }
                            ?>
                            <button id="btn-follow-<?= $userProfile['User']['id'] ?>" class="btn <?= ($isFollowing) ? ' btn-danger' : 'btn-primary' ; ?>" 
                            onclick="followUser('<?= $userProfile['User']['id'] ?>')" href="#"><?= ($isFollowing) ? '<i class="fas fa-user-minus"></i> Unfollow' : '<i class="fas fa-user-plus"></i> Follow' ; ?></button> 
                        <?php endif;?>
                            <?php 
                                if ($this->Session->read('User.id') == $userProfile['User']['id']) {
                                    echo $this->Html->link( 'Settings',
                                    array('controller' => 'users', 'action' => 'settings'),
                                    array('class' => 'btn btn-secondary mr-2')); 

                                    echo $this->Html->link( 'My Network',
                                    array('controller' => 'users', 'action' => 'network'),
                                    array('class' => 'btn btn-primary mr-2')); 

                                    echo $this->Html->link( "Create Post",
                                    array('controller' => 'posts', 'action' => 'create'),
                                    array('class' => 'btn btn-success')); 
                                }
		                    ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>