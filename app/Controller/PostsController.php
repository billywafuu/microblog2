<?php
/**
 * The controller that manages Posts.
 * 
 * @package app.Controller
 */
class PostsController extends AppController
{
    public $helpers = array('Html', 'Form');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->loadModel('User');
    }
    /**
    * Views a post.
    *
    * @param string|null $id The id of the post to view.
    */
    public function view($id = null)
    {
        if (!$id) {
            $this->render('not_found');
            return;
        }
        $post = $this->Post->findById($id);
        if (!$post) {
            $this->render('not_found');
            return;
        }
        $this->set('post', $post);
    }
    /**
    * Creates a new post.
    *
    */
    public function create()
    {
        if ($this->request->is('post')) {
            if (
                strlen($this->request->data['title']) == 0
                && strlen($this->request->data['body']) == 0
                && empty($_FILES['postImage']['name'])
            ) {
                $this->Flash->error(
                    'There should be at least a title, a body or an image to share a post.', 
                    array(
                    "key" => "error"
                    )
                );
                return;
            }
            if (strlen($this->request->data['title']) > 140) {
                $this->Flash->error(
                    'Only maximum of 140 characters per post are allowed.', 
                    array(
                        "key" => "error"
                    )
                );
                return;
            }
            if (strlen($this->request->data['body']) > 140) {
                $this->Flash->error(
                    'Only maximum of 140 characters per post are allowed.',
                    array(
                        "key" => "error"
                    )
                );
                return;
            }
            $post = array(
                "Post" => array(
                    "title"   => $this->request->data['title'],
                    "body"    => nl2br($this->request->data['body']),
                    "user_id" => $this->Auth->user('id'),
                    "created" => $st
                )
            );
            if (!$this->Post->save($post)) {
                $this->Flash->error(
                    'There has been an error while trying to save your post.', array(
                    "key" => "error"
                    )
                );
                return;
            }
            if (empty($_FILES['postImage']['name'])) {
                $this->Flash->success(
                    'The post has been shared on your profile and can be seen by your followers.',
                    array(
                        "key" => "success"
                    )
                );
                $this->redirect(array('controller' => '/','action' => 'index'));
                return;
            }

            if (!getimagesize($_FILES['postImage']["tmp_name"])) {
                $this->Flash->set('Please only upload image files for your post.');
                return;
            }

            $uploadPath = 'post/';
            $imageFileType = strtolower(
                pathinfo(
                    $uploadPath . basename($_FILES['postImage']["name"]),
                    PATHINFO_EXTENSION
                )
            );

            if (
                $imageFileType != "jpg"
                && $imageFileType != "png"
                && $imageFileType != "jpeg"
                && $imageFileType != "gif"
            ) {
                $this->Flash->set('You can only upload .jpg, .png and .jpeg files.');
                return;
            }
            $post_id = $this->Post->id;
            $fileName = $post_id . "." . $imageFileType;
            if (
                move_uploaded_file(
                    $_FILES['postImage']['tmp_name'],
                    $uploadPath . $fileName
                )
            ) {
                $this->Post->id = $post_id;
                $image = array(
                    "Post" => array(
                        "image_file_type" => $imageFileType,
                    )
                );
                $this->Post->save($image);
            } else {
                $this->Flash->set(
                    'There has been a problem while trying to upload the image.'
                );
            }
            $this->Flash->success(
                'The post has been shared on your profile and can be seen by your followers.', 
                array(
                "key" => "success"
                )
            );
            $this->redirect(array('controller' => 'users','action' => 'profile'));
        }
    }
    /**
    * Views a post.
    *
    * @param string|null $id The id of the post to edit.
    */
    public function edit($id = null)
    {
        if ($this->request->is(array('post', 'put'))) {
            $this->Post->id = $id;
            $post = $this->Post->findById($id);
            if (!$post || $post['Post']['deleted'] == 1) {
                $this->Flash->error(
                    'The post that you are trying to edit was not found.',
                    array(
                        "key" => "error"
                    )
                );
                $this->redirect(array('controller' => '/','action' => 'profile'));
                $this->autoRender = false;
                return;
            }
            if ($post['Post']['user_id'] != $this->Auth->user('id')) {
                $this->redirect(array('controller' => '/','action' => 'profile'));
                $this->autoRender = false;
                return;
            }
            if (
                strlen($this->request->data['title']) == 0
                && strlen($this->request->data['body']) == 0
                && $post['Post']['image_file_type'] == ""
            ) {
                $this->Flash->error(
                    'The title and the post body cannot be empty.',
                    array(
                        "key" => "error"
                    )
                );
                return;
            }
            if (strlen($this->request->data['title']) > 140) {
                $this->Flash->error(
                    'Only maximum of 140 characters per post are allowed.',
                    array(
                        "key" => "error"
                    )
                );
                return;
            }
            if (strlen($this->request->data['body']) > 140) {
                $this->Flash->error(
                    'Only maximum of 140 characters per post are allowed.',
                    array(
                        "key" => "error"
                    )
                );
                return;
            }
            $this->Post->id = $post['Post']['id'];
            $this->request->data['title'] = $this->request->data['title'];
            $this->request->data['body'] = nl2br($this->request->data['body']);
            if ($this->Post->save($this->request->data)) {
                if (empty($_FILES['postImage']['name'])) {
                    $this->Flash->success(
                        'The post has been successfully updated. The image attached was preserved.', 
                        array(
                        "key" => "success"
                        )
                    );
                    $this->redirect(
                        array("controller" => "posts",
                            "action" => "view",
                            $this->Post->id
                        )
                    );
                    return;
                }
                if (!getimagesize($_FILES['postImage']["tmp_name"])) {
                    $this->Flash->set(
                        'Please only upload image files for your post.'
                    );
                    return;
                }
                $uploadPath = 'post/';
                $imageFileType = strtolower(
                    pathinfo(
                        $uploadPath . basename($_FILES['postImage']["name"]),
                        PATHINFO_EXTENSION
                    )
                );
    
                if (
                    $imageFileType != "jpg"
                    && $imageFileType != "png"
                    && $imageFileType != "jpeg"
                    && $imageFileType != "gif"
                ) {
                    $this->Flash->error(
                        'You can only upload .jpg, .png and .jpeg files.',
                        array(
                            'key' => 'error'
                        )
                    );
                    return;
                }
                $post_id = $post['Post']['id'];
                $fileName = $post_id . "."  . $imageFileType;
                $newFile = $uploadPath  . $fileName;

                if (file_exists($newFile)) {
                    unlink($newFile);
                }

                if (
                    move_uploaded_file(
                        $_FILES['postImage']['tmp_name'],
                        $uploadPath . $fileName
                    )
                ) {
                    $image = array(
                        "Post" => array(
                            "image_file_type" => $imageFileType,
                        )
                    );
                    $this->Post->id = $post_id;
                    $this->Post->save($image);
                        $this->Flash->success(
                            'The post has been successfully updated. 
                            A new image was attached to the post.',
                            array(
                                "key" => "success"
                            )
                        );
                        $this->redirect(
                            array(
                            "controller" => "posts",
                            "action" => "view",
                            $this->Post->id
                            )
                        );
                } else {
                    $this->Flash->set(
                        'There has been a problem while trying to upload the image.'
                    );
                    return;
                }

                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(
                'There seems to be a problem when updating your post. Please try again.', 
                array(
                    "key" => "error"
                )
            );
            $this->redirect(
                array(
                    "controller" => "posts",
                    "action" => "edit",
                    $this->Post->id
                )
            );
        }
        if (!$id) {
            $this->Flash->error(
                'The post you are trying to edit was not found.',
                array(
                    "key" => "general_error"
                )
            );
            $this->redirect(
                array(
                    "controller" => "/",
                    "action" => "profile"
                )
            );
        }
        $post = $this->Post->findById($id);
        if (!$post || $post['Post']['deleted'] == 1) {
            $this->Flash->error(
                'The post you are trying to edit was either deleted or does not exist.', 
                array(
                    "key" => "error"
                )
            );
            $this->redirect(array('controller' => '/','action' => 'profile'));
            $this->autoRender = false;
            return;
        }
        if ($post['Post']['user_id'] != $this->Auth->user('id')) {
            $this->Flash->error(
                'The post that you are trying to edit does not belong to you.',
                array(
                    "key" => "error"
                )
            );
            $this->redirect(array('controller' => '/','action' => 'profile'));
            $this->autoRender = false;
            return;
        }
        $this->set('postData', $post);
    }
    /**
    * Deletes a post. This action only receives AJAX with a
    * payload of post_id.
    *
    */
    public function delete()
    {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $id = $this->request->data['post_id'];
            if (empty($id)) {
                return json_encode(
                    array(
                        "result" => "delete_failed",
                        "description" => "No post id submitted."
                    )
                );
            }

            $post = $this->Post->findById($id);

            if (empty($post)) {
                return json_encode(
                    array(
                        "result" => "delete_failed",
                        "description" => "Post does not exist."
                    )
                );
            }

            if ($post['Post']['deleted'] == 1) {
                return json_encode(
                    array(
                        "result" => "delete_failed",
                        "description" => "Post does not exist."
                    )
                );
            }

            if($post['Post']['user_id'] != $this->Auth->user('id')) {
                return json_encode(
                    array(
                        "result" => "unauthorized_operation",
                        "description" => "You 
                        do not own the post that you want to delete."
                    )
                );
            }
            $this->Post->id = $id;
            $delete = array(
                "deleted" => 1,
                "deleted_at" => date("Y-m-d")
            );
            $this->Post->save($delete);
            $this->autoRender = false;
            return json_encode(
                array(
                    "result" => "delete_success",
                    "description" => "The post was successfully deleted."
                )
            );
            return;
        }

    }
    /**
    * Retweets a post.
    *
    */
    public function retweet()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $id = $this->request->data['post_id'];
            if (empty($id)) {
                return json_encode(
                    array(
                        "result" => "retweet_failed",
                        "description" => "No post id submitted."
                    )
                );
            }
            $post = $this->Post->findById($id);
            if (empty($post)) {
                return json_encode(
                    array(
                        "result" => "post_not_found",
                        "description" => "Post does not exist."
                    )
                );
            }
            if($post['Post']['deleted'] == 1) {
                return json_encode(
                    array(
                        "result" => "post_not_found",
                        "description" => "Post does not exist."
                    )
                );
            }
            $post = array(
                "Post" => array(
                    "title"   => '',
                    "body"    => nl2br($this->request->data['input']),
                    "user_id" => $this->Auth->user('id'),
                    "post_id" => $id,
                    "created" => date('Y-m-d H:i:s')
                 )
             );
            if ($this->Post->save($post)) {
                return json_encode(
                    array(
                        "result" => "retweet_success",
                        "description" => "The post was successfully retweeted."
                    )
                );
            }
        }
    }
    /**
    * Real-time retrieve a posts from the users that the current user
    * is following. This action receives AJAX with a timestamp and page as
    * payload.
    * 
    */
    public function retrieve()
    {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            if (!$this->Auth->user('id')) {
                return json_encode(
                    array(
                        "result" => "failed",
                        "description" => "You must be logged in to retrieve posts."
                    )
                );
            }
            if (empty($this->request->data)) {
                return json_encode(
                    array(
                        "result" => "failed",
                        "description" => "No data sent to the server."
                    )
                );
            }
            $this->loadModel('Follow');
            $this->loadModel('Like');
            $this->loadModel('Post');
            $html = "";
            $Views = new View($this, false);
            $currentPage = 1;
            $overallPosts = count(
                $this->Follow->getFollowingPost(
                    $this->Auth->user('id')
                )
            );
            if ($overallPosts == 0) {
                return json_encode(
                    array(
                        "result" => "no_posts",
                        "html" => $Views->element('Posts/hp_no_posts')
                    )
                );
            }
            $maxNumberOfPages = ceil($overallPosts / MAX_POSTS_PER_PAGE);
            //-------------------------------------PAGE REQUEST----------------------------------------
            if (isset($this->request->data['page'])) {
                $currentPage = $this->request->data['page'];
                if ($currentPage <= 0) {
                    $currentPage = 1;
                }
                if ($currentPage > $maxNumberOfPages) {
                    $currentPage = $maxNumberOfPages;
                }
                $offset = (($currentPage - 1) * MAX_POSTS_PER_PAGE);
                $posts = $this->Follow->getFollowingPost(
                    $this->Auth->user('id'),
                    MAX_POSTS_PER_PAGE,
                    $offset
                );
                $lockTimestamp = 0;
                foreach ($posts as $post) {
                    $likes = $this->Like->getLikes($post['Post']['id']);
                    if ($lockTimestamp == 0) {
                        $lockTimestamp = strtotime($post['Post']['created']);
                    }
                    $html = $html . $Views->element(
                        'Posts/post',
                        array(
                            'post' => $post['Post'],
                            'owner' => $post['Owner'],
                            'follow' => $post['Follow'],
                            'retweet' => $post['Retweet'],
                            'likes' => $likes
                        )
                    );
                }
                $endOfFeeds = ($maxNumberOfPages == $currentPage) ? true : false ;
                return json_encode(
                    array(
                        "result" => "new_posts",
                        "html" => $html,
                        "currentPage" => $currentPage,
                        "endOfFeeds" => $endOfFeeds,
                        "timestamp" => $lockTimestamp
                    )
                );
            }
            //-------------------------------------TIMESTAMP-------------------------------------------
            if (isset($this->request->data['ts'])) {
                $posts = $this->Follow->getPostsByTimestamp(
                    $this->Auth->user('id'),
                    $this->request->data['ts'],
                    20
                );
                if (count($posts) == 0) {
                    return json_encode(
                        array(
                            "result" => "no_posts",
                            "description" => "No updated posts.",
                            "date_request" => gmdate("Y-m-d H:i:s", $this->request->data['ts'])
                        )
                    );
                }
                $html = "";
                $Views = new View($this, false);
                $lockTimestamp = 0;
                foreach ($posts as $post) {
                    $likes = $this->Like->getLikes($post['Post']['id']);
                    if ($lockTimestamp == 0) {
                        $lockTimestamp = strtotime($post['Post']['created']);
                    }
                    $html = $html . $Views->element(
                        'Posts/post',
                        array(
                            'post' => $post['Post'],
                            'owner' => $post['Owner'],
                            'follow' => $post['Follow'],
                            'retweet' => $post['Retweet'],
                            'likes' => $likes
                        )
                    );
                }
                return json_encode(
                    array(
                        "result" => "new_posts",
                        "description" => "There are new posts.",
                        "html" => $html,
                        "timestamp" => $lockTimestamp
                    )
                );
            }
        }
        $this->redirect(array('controller' => '/','action' => 'profile'));
    }
}