<div class="container-fluid">
        <div class="card border-0">
            <div class="card-body">
                <h5><i class="fas fa-sad-tear"></i> No user is following you</h5>
                <p> No one is following you? Don't be sad though. It will come!</p>
                <small class="text-muted"> Tip: Tell your friends that you are here on Microblog! Invite them!</small>
            </div>
        </div>
</div>